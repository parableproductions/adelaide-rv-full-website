<?php
the_post();
get_header(); ?>

<section class="helptips" id="helptips">
    <div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;"></div>
    <div class="container">
        <div class="helptips__introduction">
            <h1><?php single_term_title(); ?></h1>
            <?php echo term_description() ?>
        </div>

        <!-- FILTER -->
        <div class="helptips__navigation">
            <div class="row">
                <?php $args = array(
"type" => "articles",   
"taxonomy" => "articles_category",   
"orderby" => "name",
"order" => "ASC" );
      $categories = get_categories($args);
?>
                <?php foreach ($categories as $category) : ?>
                <div class="col-md-3">
                    <a href="/articles_category/<?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
                </div>
                <?php endforeach; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
        <!-- /FILTER -->



        <div class="helptips__content">
            <div class="row">

                <?php if ( have_posts() ) : ?>



                    <div class="col-sm-12 col-md-6 col-lg-3">
                    <div class="helptips__content--wrap">
                        <div class="top-image">
                            <div class="overlay-bg"></div>
                            <?php if ( has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail(); ?>
                            <?php endif; ?>
                            <!-- <img class="help-img" src="<?php lp_image_dir(); ?>/video.jpg"> -->
                            <div class="published-date"><?php echo get_the_date(); ?></div>
                        </div>
                        <div class="infor-section">
                            <h5 class="help-title"><?php the_title();?></h5>
                            <?php $excerpt = get_field('articles_content'); ?>

                            <p> <?php echo wp_trim_words( $excerpt, 10 ); ?></p>
                            <a href="<?php the_permalink(); ?>" target="_blank"> More Details</a>
                        </div>
                    </div>
                </div>



                <?php while (have_posts()) : the_post(); ?>
                <div class="col-sm-12 col-md-6 col-lg-3">
                    <div class="helptips__content--wrap">
                        <div class="top-image">
                            <div class="overlay-bg"></div>
                            <?php if ( has_post_thumbnail()) : ?>
                            <?php the_post_thumbnail(); ?>
                            <?php endif; ?>
                            <!-- <img class="help-img" src="<?php lp_image_dir(); ?>/video.jpg"> -->
                            <div class="published-date"><?php echo get_the_date(); ?></div>
                        </div>
                        <div class="infor-section">
                            <h5 class="help-title"><?php the_title();?></h5>
                            <?php $excerpt = get_field('articles_content'); ?>

                            <p> <?php echo wp_trim_words( $excerpt, 10 ); ?></p>
                            <a href="<?php the_permalink(); ?>" target="_blank"> More Details</a>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>

    </div>
</section>
<?php get_footer(); ?>