	<footer class="site-footer">
	    <div class="site-footer__footer">
	        <div class="site-footer__footer--menuitems">
	            <div class="container pad-0">
	                <div class="row">
	                    <div class="col-xs-12 col-sm-4 col-md-3">
	                        <div class="menu-title">
	                            <h4>Adelaide RV</h4>
	                        </div>
	                        <ul>
	                            <?php
							if( have_rows('adelaide_rv','option') ):
								while( have_rows('adelaide_rv','option') ) : the_row();
								?>
	                            <li><a href="<?php echo get_site_url();
										the_sub_field('link','option')?>"><?php the_sub_field('title','option');?></a>
	                            </li>
	                            <?php endwhile;
						endif;?>
	                            <li><a href="#" data-toggle="modal" data-target="#privacyModal">Privacy Policy</a></li>
	                            <li><a href="#" data-toggle="modal" data-target="#termsModal">Terms & Conditions</a></li>
	                        </ul>
	                    </div>
	                    <div class="col-xs-12 col-sm-4 col-md-3">
	                        <div class="menu-title">
	                            <h4>Our Brands</h4>
	                        </div>
	                        <ul>
							<?php
							if( have_rows('our_brands','option') ):
								while( have_rows('our_brands','option') ) : the_row();
								?>
	                            <li><a href="<?php echo get_site_url();
										the_sub_field('link','option')?>"><?php the_sub_field('title','option');?></a></li>
										          <?php endwhile;
						endif;?>
	                        </ul>
	                    </div>
	                    <div class="col-xs-12 col-sm-4 col-md-3">
	                        <div class="menu-title">
	                            <h4>Services</h4>
	                        </div>
	                        <ul>
							<?php
							if( have_rows('services','option') ):
								while( have_rows('services','option') ) : the_row();
								?>
	                            <li><a href="<?php echo get_site_url();
										the_sub_field('link','option')?>"><?php the_sub_field('title','option');?></a></li>
								<?php endwhile;
						endif;?>
	                        </ul>
	                    </div>
	                    <div class="col-xs-12 col-sm-4 col-md-3">
	                        <div class="logo">
	                            <a href="#" target="_blank"><img src="<?php lp_image_dir(); ?>/adelaide-rv-logo_white.png"
	                                    width="224"></a>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div class="site-footer__footer--bottom">
	            <div class="container pad-0">

	            	<div class="row">
	            		<div class="col-sm-12 col-md-6">
	            			<div class="left-panel">
			                    <div class="left-panel__title">
			                        <h5>Get deals and the latest news to your inbox</h5>
			                    </div>
			                    <div class="left-panel__email-box">
			                        <?php echo do_shortcode ('[ninja_form id=3]') ?>
			                    </div>
			                </div>
	            		</div>
	            		
						<!-- Visitor Counter Shortcode -->
	            		<!-- <div class="col-sm-12 col-md-3">
	            			<div class="right-panel">
			                	<div class="visitors-counter">
		                            <?php echo do_shortcode('[visitors]');?>
		                        </div>
			                </div>
	            		</div> -->
	            		<div class="col-sm-12 col-md-3">
	            			<div class="social ml-auto">
		                        <a href="https://www.facebook.com/adelaiderv/" target="_blank"><i
		                                class="fab fa-facebook-f fa-2x"></i></a>
		                        <a href="https://www.instagram.com/explore/tags/adelaiderv/?hl=en" target="_blank"><i
		                                class="fab fa-instagram fa-2x"></i></a>
		                    </div>
	            		</div>
	            	</div>
	            </div>
	        </div>
	        <div class="site-footer__footer--copyright">
	            <div class="container pad-0">
            		<div class="wrap">
            			<span class="wrap-copy">&#169; Adelaide RV 2020</span>
                    </div>
	            </div>
	        </div>
	</footer>
	</div> <!-- end wrapper -->

	<!-- Modal: Terms & Conditions -->
	<div class="modal fade" id="termsModal">
	    <div class="modal-dialog modal-dialog-centered">
	        <div class="modal-content">
	            <div class="modal-body terms-modal">
	                <?php the_field('terms_conditions','option');?>
	            </div>
	        </div>
	    </div>
	</div>

	<!-- Modal: Terms & Conditions -->
	<div class="modal fade" id="privacyModal">
	    <div class="modal-dialog modal-dialog-centered">
	        <div class="modal-content">
	            <div class="modal-body terms-modal">
	                <?php the_field('privacy_policy','option');?>
	            </div>
	        </div>
	    </div>
	</div>

	<?php wp_footer(); ?>
	</body>

	</html>