jQuery(document).ready(function($){
    $(document.body).removeClass('no-js').addClass('js');

    initPolyfills();

    // Utilities
    initClassToggle();
    initAnchorScroll();
    initLandingVideo();
    initSliderTop();
    initSliderBottom();
    initClassIframeVideo();
    initRangeInternalSlider();
    //initRangeExternalSlider();
    initDownUp();
    //initDownUpOption();
    //initDownUpSpec();
    initFaq();

    // CF7 Form Control
    //initCF7();
    //initFullheightMobile();
});



function initDownUp() {
$("#range-title").click(function(){
    $("#range-wrap").slideToggle("slow");
  });
$("#range-option").click(function(){
    $("#range-wrap-option").slideToggle("slow");
  });
$("#range-specifications").click(function(){
    $("#range-wrap-specifications").slideToggle("slow");
  });
}


function initFaq() {
$("#services_question1").click(function(){
    $("#services_answer1").slideToggle("slow");
  });
$("#services_question2").click(function(){
    $("#services_answer2").slideToggle("slow");
  });
$("#services_question3").click(function(){
    $("#services_answer3").slideToggle("slow");
  });
$("#services_question4").click(function(){
    $("#services_answer4").slideToggle("slow");
  });
$("#services_question5").click(function(){
    $("#services_answer5").slideToggle("slow");
  });
}



function isMobile() {
    return window.matchMedia('(max-width:767px)').matches;
}

function initPolyfills() {
    // CSS object-fit for IE
    objectFitImages();

    // polyfill for IE - startsWith
    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        };
    }

    // polyfill for IE - forEach
    if ('NodeList' in window && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = function (callback, thisArg) {
          thisArg = thisArg || window;
          for (var i = 0; i < this.length; i++) {
            callback.call(thisArg, this[i], i, this);
          }
        };
    }
}
//--- Iframe----->
function initClassIframeVideo() {
    $(document).ready(function() {
        var url = $("#iframeModal").attr('src');
        $("#videoModal").on('hide.bs.modal', function() {
            $("#iframeModal").attr('src', '');
        });
        $("#videoModal").on('show.bs.modal', function() {
            $("#iframeModal").attr('src', url);
        });
    });
}
// end of Iframe ---->

/**
 * Toggle class on click
 */
function initClassToggle() {
    $(document.body).on('click', '[data-toggle="class"][data-class]', function(event) {
        var $trigger = $(this);
        var $target = $($trigger.data('target') ? $trigger.data('target') : $trigger.attr('href'));

        if($target.length) {
            event.preventDefault();
            $target.toggleClass($trigger.data('class'));
            $trigger.toggleClass('classed');
        }
    });
}

function initLandingVideo() {
    $('.landing__video').each(function () {
        var $container = $(this);
        var $tmpl = $container.find('[type="text/template"]');

        if ($tmpl.length) {
            $container.html($tmpl.html()); // This is so the video starts loading after everything else has

            var $iframe = $container.find('iframe');

            if ($iframe.length) {
                // Resize the iframe to cover the container
                cover($container, $iframe.attr('width'), $iframe.attr('height'));

                // Create the player object and make the video visible once it starts playing
                var player = new Vimeo.Player($iframe.get(0));
                $container.addClass('landing__video--loaded');
                player.on('timeupdate', function () {
                    $container.addClass('landing__video--playing');
                });
            }

        }
    });

    

    function cover($target, width, height) {
        var $panel = $target.parents('.landing');

        var winWidth = $panel.width();
        var winHeight = $panel.height();

        var ratio = height / width;
        var winRatio = winHeight / winWidth;

        if (winRatio > ratio) {
            $target.width(width * (winHeight / height));
            $target.height(winHeight);

            $target.css('top', 0);
            $target.css('left', (winWidth - $target.width()) / 2);
        } else {
            $target.width(winWidth);
            $target.height(height * (winWidth / width));

            $target.css('top', (winHeight - $target.height()) / 2);
            $target.css('left', 0);
        }
    }
}

 //section  Fade in
$(document.body).toggleClass('no-js js');
var menuHeight = 70;
if(window.matchMedia('(max-width:991px)').matches){
    menuHeight = 40;
}

var fadeElements = $('.fadein');
var winHeight = $(window).height();
$(window).scroll(function() {
    var scrollTop = $(window).scrollTop();

    fadeElements.each(function(){
        if(scrollTop > ($(this).offset().top - winHeight + (winHeight/6))){
            $(this).addClass('in');
        }else{
            $(this).removeClass('in');
        }
    });
});


/**
 * sliders
 */
function initSliderTop() {
  $('.gallery-slider__wrapper--top').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    arrows:false,
    adaptiveHeight: true,
    asNavFor: '.gallery-slider__wrapper--bottom'
  });
}


function initSliderBottom() {
  $('.gallery-slider__wrapper--bottom').slick({
    slidesToShow: 4,
    slidesToScroll: 2,
    arrows:false,
    centerMode: false,
   focusOnSelect: true,

    asNavFor: '.gallery-slider__wrapper--top',
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        centerMode: false,
        dots: false
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        centerMode: true,
        dots: false
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
  });
}

function initRangeInternalSlider() {
$('.range-internal-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    adaptiveHeight: true,
    prevArrow:"<div class='arrow-wrap'><a class='slick-prev-one'> <i class='fas fa-chevron-left'></i></a></div>",
    nextArrow:"<div class='arrow-wrap'><a class='slick-next-one'> <i class='fas fa-chevron-right'></i></a></div>",
});
}

function initRangeExternalSlider() {
$('.range-external-slider').slick({
    arrows:false,
    autoplay: true,
    autoplaySpeed: 3000,
    adaptiveHeight: true,
    prevArrow:"<a class='slick-prev'> <i class='fas fa-chevron-left'></i></a>",
    nextArrow:"<a class='slick-next'> <i class='fas fa-chevron-right'></i></a>",
});
}

function initRangeFloorplanSlider() {
$('.range-floorplan-slider').slick({
    arrows:false,
    autoplay: true,
    autoplaySpeed: 3000,
    adaptiveHeight: true,
    prevArrow:"<a class='slick-prev'> <i class='fas fa-chevron-left'></i></a>",
    nextArrow:"<a class='slick-next'> <i class='fas fa-chevron-right'></i></a>",
});
}


/**
 * Smooth anchor scrolling
 */
function initAnchorScroll() {
    $('a[href*="#"]:not([data-toggle])').click(function(event) {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name="'+this.hash.slice(1)+'"]');
            if (target.length && !target.parents('.woocommerce-tabs').length) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
            }
        }
    });
}

// function initCF7(){
//     var wpcf7Elm = $('.wpcf7-form')[0];
//     var formbtn = $('.wpcf7-form input[type="submit"]');

//     formbtn.on('click', function(){
//         formbtn.val('SENDING...');
//     });

//     document.addEventListener( 'wpcf7invalid', function( event ) {
//         formbtn.val('SUBMIT');
//     }, false );

//     document.addEventListener( 'wpcf7mailsent', function( event ) {
//         formbtn.val('SENT!');
//     }, false );
// }

/*function initFullheightMobile() {
    // Fix mobile 100vh change on address bar show/hide
    var lastHeight = $(window).height();
    var heightChangeTimeout = undefined;
    if(isMobile()) {
        $('.vh').css('height', lastHeight);
    }
    (maybe_update_landing_height = function() {
        var winHeight = $(window).height();

        if(heightChangeTimeout !== undefined) {
            clearTimeout(heightChangeTimeout);
        }

        if(!isMobile()) {
            $('.vh').css('height', '');
        }
        else if(Math.abs(winHeight - lastHeight) > 100) {
            heightChangeTimeout = setTimeout(function() {
                var winHeight = $(window).height();
                $('.vh').css('height', winHeight);
                lastHeight = winHeight;
            }, 50);
        }
    })();
    $(window).resize(maybe_update_landing_height);
}*/

/*function initCopyToClipboard() {
  $(document.body).on('click', '[data-copy-to-clipboard]', function(event) {
    event.preventDefault();

    // Source: https://hackernoon.com/copying-text-to-clipboard-with-javascript-df4d4988697f
    var el = document.createElement('textarea');
    el.value = $(this).data('copy-to-clipboard');
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
  });
}*/
