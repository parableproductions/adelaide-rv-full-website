(function($){ 
    
    $(document).ready(function(){ 
        $(document).on('click','.js-filter-item2', function(e){ 
            e.preventDefault();

    var category = $(this).data('category');

    $.ajax({ url: wpAjax2.ajaxUrl2, 
        data: {action: 'filter2', category: category}, 
        type: 'post', 
        success: function (result) { 
            $('.js-filter2').html(result); 
        },
    error: function(result) { 
        console.warn(result); }
    });
   

});
});
   

}) (jQuery);