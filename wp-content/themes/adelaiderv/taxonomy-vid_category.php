<?php
the_post();
get_header(); ?>

<section class="helptips" id="helptips">
    <div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;"></div>
    <div class="container">
        <div class="helptips__introduction">
            <h1><?php single_term_title(); ?></h1>
            <?php echo term_description() ?>
        </div>

        <!-- FILTER -->
        <div class="video-tutorials__navigation">
            <div class="row">
                <?php $args = array(
"type" => "videos",   
"taxonomy" => "video_category",   
"orderby" => "name",
"order" => "ASC" );
      $categories = get_categories($args);
?>
                <?php foreach ($categories as $category) : ?>
                <div class="col-md-3">
                    <a href="/video_category/<?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
                </div>
                <?php endforeach; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
        <!-- /FILTER -->



        <div class="video-tutorials__video">
            <!--<div class="videos-list">
                <?php echo do_shortcode ('[huge_it_videogallery id="1"]') ?>
            </div>-->
            <div class="row">

                <?php if ( have_posts() ) : ?>

                <div class="col-sm-6 col-md-3">
                    <div class="video-tutorials__video--list">
                        <a href="#" data-toggle="modal" data-target="#videoModal">
                            <div class="list-wrapper">
                                <?php if ( has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail(); ?>
                                <?php endif; ?>
                                <div class="videos-overlay"></div>
                                <!-- <img class="videos-logo" src="<?php lp_image_dir(); ?>/adelaide-rv-logo_white.png">-->
                                <h5 class="videos-title"><?php the_title();?></h5>
                            </div>
                        </a>
                    </div>
                </div>



                <?php while (have_posts()) : the_post(); ?>
                <div class="col-sm-6 col-md-3">
                    <div class="video-tutorials__video--list">
                        <a href="#" data-toggle="modal" data-target="#videoModal">
                            <div class="list-wrapper">
                                <?php if ( has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail(); ?>
                                <?php endif; ?>
                                <div class="videos-overlay"></div>
                                <!-- <img class="videos-logo" src="<?php lp_image_dir(); ?>/adelaide-rv-logo_white.png">-->
                                <h5 class="videos-title"><?php the_title();?></h5>
                            </div>
                        </a>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="videoModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <iframe id="iframeModal" width="560" height="315" src="<?php the_field('single_video'); ?>?autoplay=1"
                    frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>