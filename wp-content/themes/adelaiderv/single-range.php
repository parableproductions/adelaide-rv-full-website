<?php 
the_post();
get_header(); ?>

<section class="range" id="range">
    <?php if ( has_post_thumbnail() ) { ?>
    <div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
    </div>
    <?php } else { ?>
    <div class="top-header" id="top-header"
        style="background: url(<?php lp_image_dir(); ?>/lifestyle_02.jpg) no-repeat center/cover;"></div>
    <?php } ?>

    <div class="container">
        <div class="range__introduction">
            <h1><?php the_title();?></h1>
            <?php the_field('intro');?>
        </div>

        <!-- We have add all the images internal and external in one slide -->
        <div class="range__internal">
            <div class="range__internal--gallery">
                <?php $internalImages = get_field('gallery');
                        if( $internalImages ): ?>
                <div class="range-internal-slider">
                    <?php foreach( $internalImages as $internalImage ): ?>
                    <img src="<?php echo esc_url($internalImage['sizes']['large']); ?>" />
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <!-- slide end-->

        <!-- Layout Start-->
        <?php if( have_rows('floorplans_repeater') ): ?>
        <div class="range__layout">
             <div id="range-title" class="range__layout--title"><h5>Floorplan</h5>
                <i class="fas fa-angle-down"></i>
             </div>
             <div id="range-wrap" class="range__layout--wrap hide">
             <?php while( have_rows('floorplans_repeater') ): the_row(); ?>

                 <div class="range__layout--content">
                     <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <img src="<?php the_sub_field('floorplan_image'); ?>">
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="wrap">
                                <div class="plan-title"><h5><?php the_sub_field('title');?></h5></div>
                                <div class="plan-bodysize">
                                <?php the_sub_field('specifications'); ?>
                                </div>
                            </div>
                        </div>
                     </div>
                 </div>
                 <?php endwhile; ?>
             </div>
        </div>
        <?php endif; ?>

        <!-- Layout End-->

        <!-- Option Start -->
        <?php if( have_rows('options') ): ?>

        <div  class="range__layout options">
            <div id="range-option" class="range__layout--title"><h5>Options</h5>
                <i class="fas fa-angle-down"></i>
            </div>
            <div id="range-wrap-option" class="range__layout--wrap hide">
            <?php while( have_rows('options') ): the_row(); ?>
                 <div class="range__layout--content">
                     <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <img src="<?php the_sub_field('image'); ?>">
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="wrap">
                                <div class="plan-title"><h5><?php the_sub_field('title');?></h5></div>
                               <div class="plan-bodysize">
                               <?php the_sub_field('specifications'); ?>
                                </div>
                            </div>
                        </div>
                     </div>
                 </div>
                 <?php endwhile; ?>

             </div>
        </div>
        <?php endif; ?>

        <!-- Option Start -->

        <!-- Specifications Start -->
        <div class="range__layout specifications">
             <div id="range-specifications" class="range__layout--title"><h5>Specifications</h5>
                <i class="fas fa-angle-down"></i>
             </div>
             <div id="range-wrap-specifications" class="range__layout--wrap hide">
                 <?php the_field('specifications');?>
            </div>
        </div>

        <!-- Specifications End -->

        <!-- Specifications Start -->
         <!-- <div class="range__layout specifications">
             <div id="range-specifications" class="range__layout--title"><h5>Specifications</h5>
                <i class="fas fa-angle-down"></i>
             </div>
             <div id="range-wrap-specifications" class="range__layout--wrap">
                 <div class="range__layout--content">
                    <div class="item">
                        <p>Total Length</p>
                    </div>
                    <div class="description">
                        <p>Length 7140mm</p>
                    </div>
                 </div>
                 <div class="range__layout--content">
                    <div class="item">
                        <p>Total Length</p>
                    </div>
                    <div class="description">
                        <p>Length 7140mm</p>
                    </div>
                 </div>
                 <div class="range__layout--content">
                    <div class="item">
                        <p>Total Length</p>
                    </div>
                    <div class="description">
                        <p>Length 7140mm</p>
                    </div>
                 </div>
                 <div class="range__layout--content">
                    <div class="item">
                        <p>Total Length</p>
                    </div>
                    <div class="description">
                        <p>Length 7140mm</p>
                    </div>
                 </div>
                 <div class="range__layout--content">
                    <div class="item">
                        <p>Total Length</p>
                    </div>
                    <div class="description">
                        <p>Length 7140mm</p>
                    </div>
                 </div>
            </div>
        </div> -->
        <!-- Specifications End -->

        <!-- Download Broucher Start -->
        <div class="range__url">
            <?php if( get_field('brochure') ): ?>
                <div class="range__url--wrap">
                    <a target="_blank" href="<?php the_field('brochure'); ?>"><h5>Download Brochure</h5></a>
                </div>
            <?php endif; ?>
        </div>
        <!-- Download Broucher End -->

        <!-- Virtual Tour Start -->
        <div class="range__url">
            <?php if( get_field('virtual_tour') ): ?>
                <div class="range__url--wrap">
                    <a target="_blank" href="<?php the_field('virtual_tour'); ?>"><h5>Virtual Tour</h5></a>
                </div>
            <?php endif; ?>
        </div>
        <!-- Virtual Tour End -->

         <!-- Virtual Tour Start -->
        <div class="range__url">
            <div class="range__url--wrap">
                <a href="#" data-toggle="modal" data-target="#contactModal"><h5>Contact us for more information -  Click here</h5></a>
            </div>
        </div>
        <!-- Virtual Tour End -->










        <!--<div class="range__links">
            <div class="row">

                <?php if( get_field('brochure') ): ?>
                <div class="col-sm-12 col-md-4">
                    <a class="btn btn-primary" target="_blank" href="<?php the_field('brochure'); ?>">Download
                        Brochure</a>
                </div>
                <?php endif; ?>

                <?php if( get_field('virtual_tour') ): ?>
                <div class="col-sm-12 col-md-4">
                    <a class="btn btn-primary" target="_blank" href="<?php the_field('virtual_tour'); ?>">Virtual
                        Tour</a>
                </div>
                <?php endif; ?>

                <?php if( have_rows('floorplans_repeater') ): ?>
                <div class="col-sm-12 col-md-4">
                    <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button"
                        aria-expanded="false" aria-controls="collapseExample">
                        Floorplans
                    </a>
                </div>
                <?php endif; ?>
            </div>
        </div>


        <div class="range__floorplans collapse" id="collapseExample">
            <div class="card card-body">
                <?php if( have_rows('floorplans_repeater') ): ?>
                <?php while( have_rows('floorplans_repeater') ): the_row(); ?>
                <h3><div class="title">
                    <?php the_sub_field('title');?></div></h3>
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <img src="<?php the_sub_field('floorplan_image'); ?>" />
                    </div>
                    <div class="col-sm-12 col-md-6 specificiations">
                        <?php the_sub_field('specifications'); ?>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>-->
    </div>
</section>
<!-- Modal -->
<div class="modal fade" id="contactModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <div class="modal-body">
               <div class="range__enquiry">
            <div class="range__enquiry--copy">
                <p>Would like to know more about</p>
                <h3><?php the_title();?></h3>
                </div>
                <?php echo do_shortcode('[ninja_form id=16]');?>
            </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>