<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>

	<script type="text/javascript">
		var _ajaxurl = '<?= admin_url("admin-ajax.php"); ?>';
		var _pageid = '<?= get_the_ID(); ?>';
		var _imagedir = '<?php lp_image_dir(); ?>';
	</script>
	<!--<script src="https://kit.fontawesome.com/29a98d6f5b.js" crossorigin="anonymous"></script>-->
	<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">-->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php lp_image_dir(); ?>/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php lp_image_dir(); ?>/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php lp_image_dir(); ?>/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php lp_image_dir(); ?>/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php lp_image_dir(); ?>/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php lp_image_dir(); ?>/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php lp_image_dir(); ?>/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php lp_image_dir(); ?>/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php lp_image_dir(); ?>/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php lp_image_dir(); ?>/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php lp_image_dir(); ?>/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php lp_image_dir(); ?>/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php lp_image_dir(); ?>/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php lp_image_dir(); ?>/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
</head>
<body <?php body_class(); ?>>

<?php

// This fixes an issue where wp_nav_menu applied the_title filter which causes WC and plugins to change nav menu labels
print '<!--';
the_title();
print '-->';

?>

<div id="top"></div>
<div class="wrapper">
	<header class="site-header">
		<div class="container">
			<div class="top-menu">
				<div class="left-top">
					 <a href="<?php echo get_home_url(); ?>"><img class="logo" src="<?php lp_image_dir(); ?>/adelaide-rv-logo_white.png"></a>
				</div>
				<div class="right-top">
					<p>For enquiries call us on</p>
					<a href="tel:0882818889">08 8281 8889</a>
				</div>
			</div>
			<div class="main-menu">
				<?php
				wp_nav_menu( array(
				    'theme_location' => 'header-menu',
				    'container_class' => 'custom-menu-class' ) );
				?>
			</div>
		</div>
		<div class="side-button-wrap">
		    <!--<a href="#contact-model" rel="modal:open" class="btn">ENQUIRE NOW</a>-->
		    <a href="#" data-toggle="modal" data-target="#contactModalGeneral" class="btn">ENQUIRE US NOW</a>
		</div>
	</header>
<!-- Modal -->
<div class="header-modal modal fade" id="contactModalGeneral">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <div class="modal-body">
               <div class="main-enquiry">
	            	<div class="main-enquiry__copy">
	               		 <h2>General Enquiry</h2>
	               		 <p>Would like to know more about Would like to know more about Would like to know more about</p>
	                </div>
	                <?php echo do_shortcode('[ninja_form id=5]');?>
	            </div>
            </div>
        </div>
    </div>
</div>


