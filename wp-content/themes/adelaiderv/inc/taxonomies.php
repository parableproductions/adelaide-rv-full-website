<?php
// Register videos cat taxonomy
function create_video_taxonomy() {
    
  $labels = array(
    'name' => _x( 'Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Categories' ),
    'all_items' => __( 'All Categories' ),
    'parent_item' => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item' => __( 'Edit Category' ),
    'update_item' => __( 'Update Category' ),
    'add_new_item' => __( 'Add Category' ),
    'new_item_name' => __( 'New Category Name' ),
    'menu_name' => __( 'Category' ),
  );
    
  register_taxonomy('video_category',array('videos'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
  ));

}

add_action( 'init', 'create_video_taxonomy', 0 );

// Register articles cat taxonomy
function create_articles_taxonomy() {
    
  $labels = array(
    'name' => _x( 'Categories', 'taxonomy general name' ),
    'singular_name' => _x( 'Category', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Categories' ),
    'all_items' => __( 'All Categories' ),
    'parent_item' => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item' => __( 'Edit Category' ),
    'update_item' => __( 'Update Category' ),
    'add_new_item' => __( 'Add Category' ),
    'new_item_name' => __( 'New Category Name' ),
    'menu_name' => __( 'Category' ),
  );
    
  register_taxonomy('articles_category',array('articles'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
  ));

}

add_action( 'init', 'create_articles_taxonomy', 0 );

// Register range manufacturer taxonomy
function create_manufacturer_taxonomy() {
    
  $labels = array(
    'name' => _x( 'Manufacturer', 'taxonomy general name' ),
    'singular_name' => _x( 'Manufacturer', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Manufacturers' ),
    'all_items' => __( 'All Manufacturers' ),
    'parent_item' => __( 'Parent Manufacturer' ),
    'parent_item_colon' => __( 'Parent Manufacturer:' ),
    'edit_item' => __( 'Edit Manufacturer' ),
    'update_item' => __( 'Update Manufacturer' ),
    'add_new_item' => __( 'Add Manufacturer' ),
    'new_item_name' => __( 'New Manufacturer Name' ),
    'menu_name' => __( 'Manufacturer' ),
  );
    
  register_taxonomy('manufacturer_category',array('range'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
  ));

}

add_action( 'init', 'create_manufacturer_taxonomy', 0 );