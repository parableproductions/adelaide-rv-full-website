<?php

add_action( 'wp_ajax_nopriv_filter','filter_ajax' );
add_action( 'wp_ajax_filter','filter_ajax' );

    function filter_ajax() {
     $category = $_POST['category'];
     $args = array (
         'post_type' => 'videos',
         'posts_per_page' => -1
        );


        ?>
    <div class="video-tutorials__video">
        <div class="row">
            <?php if(!empty($category)) {
                $args['tax_query'] = array(
                    array(
                        'taxonomy' => 'video_category',
                        'field'    => 'term_id',
                        'terms'    => $category ,
                    ),
                );
            }

            $query = new WP_Query($args);
            if($query->have_posts()): while($query->have_posts()) : 
                $query->the_post();?>
                <?php $videoid = get_the_ID(); ?>
                    <div class="col-sm-6 col-md-3">
                        <div class="video-tutorials__video--list">
                            <a href="#" data-toggle="modal" data-target="#videoModal<?php echo $videoid; ?>">
                                <div class="list-wrapper">
                                    <?php if ( has_post_thumbnail()) : ?>
                                    <?php the_post_thumbnail(); ?>
                                    <?php endif; ?>
                                    <div class="videos-overlay"></div>
                                    <!-- <img class="videos-logo" src="<?php lp_image_dir(); ?>/adelaide-rv-logo_white.png">-->
                                    <h5 class="videos-title"><?php the_title();?></h5>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="videoModal<?php echo $videoid; ?>">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <iframe id="iframeModal" width="850" height="450" src="<?php the_field('single_video'); ?>?autoplay=0"
                                        frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
            endwhile;
            endif;
            wp_reset_postdata(); ?>
        </div>
    </div>
<?php

 die();}