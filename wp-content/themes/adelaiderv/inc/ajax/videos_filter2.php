<?php

add_action( 'wp_ajax_nopriv_filter2','filter2_ajax' );
add_action( 'wp_ajax_filter2','filter2_ajax' );

function filter2_ajax() {
 $category = $_POST['category'];
 $args = array ( 
     'post_type' => 'articles', 
     'posts_per_page' => -1
    );


?>
<div class="helptips__content">

    <div class="row">

        <?php if(!empty($category)) {
    $args['tax_query'] = array(
        array(
            'taxonomy' => 'articles_category',
            'field'    => 'term_id',
            'terms'    => $category ,

        ),
    );

}


$query = new WP_Query($args);

if($query->have_posts()): while($query->have_posts()) : 
    $query->the_post();?>

        <div class="col-sm-12 col-md-6 col-lg-3">
            <div class="helptips__content--wrap">
                <div class="top-image">
                    <div class="overlay-bg"></div>
                    <?php if ( has_post_thumbnail()) : ?>
                    <?php the_post_thumbnail(); ?>
                    <?php endif; ?>
                    <!-- <img class="help-img" src="<?php lp_image_dir(); ?>/video.jpg"> -->
                    <div class="published-date"><?php echo get_the_date(); ?></div>
                </div>
                <div class="infor-section">
                    <h5 class="help-title"><?php the_title();?></h5>

                    <p> <?php echo wp_trim_words( get_field('articles_content'), 20, '...'); ?></p>
                    <a href="<?php the_permalink(); ?>" target="_blank"> More Details</a>
                </div>
            </div>
        </div>

        <?php 
endwhile;
endif;

wp_reset_postdata(); ?>
    </div>
</div>

<?php

 die();}