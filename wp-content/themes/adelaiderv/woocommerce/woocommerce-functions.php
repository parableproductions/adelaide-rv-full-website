<?php add_theme_support( 'woocommerce' );

// Gallery Support
add_action( 'after_setup_theme', 'yourtheme_setup' );
 
function yourtheme_setup() {
    add_theme_support( 'wc-product-gallery-zoom' );
    add_theme_support( 'wc-product-gallery-lightbox' );
    add_theme_support( 'wc-product-gallery-slider' );
}

//? ********** Register Make product cat taxonomy **********
function create_make_taxonomy() {
    
    $labels = array(
      'name' => _x( 'Manufacturer', 'taxonomy general name' ),
      'singular_name' => _x( 'Manufacturer', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Manufacturers' ),
      'all_items' => __( 'All Manufacturers' ),
      'parent_item' => __( 'Parent Manufacturer' ),
      'parent_item_colon' => __( 'Parent Manufacturer:' ),
      'edit_item' => __( 'Edit Manufacturer' ),
      'update_item' => __( 'Update Manufacturer' ),
      'add_new_item' => __( 'Add Manufacturer' ),
      'new_item_name' => __( 'New Category Manufacturer' ),
      'menu_name' => __( 'Manufacturer' ),
    );
      
    register_taxonomy('make',array('product'), array(
      'hierarchical' => true,
      'labels' => $labels,
      'show_ui' => true,
      'show_admin_column' => true,
      'query_var' => true,
    ));
  
  }
  
  add_action( 'init', 'create_make_taxonomy', 0 );

//? ********** Register Model product cat taxonomy **********
function create_model_taxonomy() {
    
    $labels = array(
      'name' => _x( 'Model', 'taxonomy general name' ),
      'singular_name' => _x( 'Model', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Models' ),
      'all_items' => __( 'All Models' ),
      'parent_item' => __( 'Parent Model' ),
      'parent_item_colon' => __( 'Parent Model:' ),
      'edit_item' => __( 'Edit Model' ),
      'update_item' => __( 'Update Model' ),
      'add_new_item' => __( 'Add Model' ),
      'new_item_name' => __( 'New Category Model' ),
      'menu_name' => __( 'Model' ),
    );
      
    register_taxonomy('model',array('product'), array(
      'hierarchical' => true,
      'labels' => $labels,
      'show_ui' => true,
      'show_admin_column' => true,
      'query_var' => true,
    ));
  
  }
  
  add_action( 'init', 'create_model_taxonomy', 0 );

  //? ********** Register Body Type product cat taxonomy **********
  function create_body_taxonomy() {
    
    $labels = array(
      'name' => _x( 'Body Type', 'taxonomy general name' ),
      'singular_name' => _x( 'Body Type', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Body Type' ),
      'all_items' => __( 'All Body Types' ),
      'parent_item' => __( 'Parent Body Type' ),
      'parent_item_colon' => __( 'Parent Body Type:' ),
      'edit_item' => __( 'Edit Body Type' ),
      'update_item' => __( 'Update Body Type' ),
      'add_new_item' => __( 'Add Body Type' ),
      'new_item_name' => __( 'New Category Body Type' ),
      'menu_name' => __( 'Body Type' ),
    );
      
    register_taxonomy('body',array('product'), array(
      'hierarchical' => true,
      'labels' => $labels,
      'show_ui' => true,
      'show_admin_column' => true,
      'query_var' => true,
    ));
  
  }

  add_action( 'init', 'create_body_taxonomy', 0 );

  //? ********** Register Condition product cat taxonomy **********
//   function create_condition_taxonomy() {
    
//     $labels = array(
//       'name' => _x( 'Condition', 'taxonomy general name' ),
//       'singular_name' => _x( 'Condition', 'taxonomy singular name' ),
//       'search_items' =>  __( 'Search Condition' ),
//       'all_items' => __( 'All Condition' ),
//       'parent_item' => __( 'Parent Condition' ),
//       'parent_item_colon' => __( 'Parent Condition:' ),
//       'edit_item' => __( 'Edit Condition' ),
//       'update_item' => __( 'Update Condition' ),
//       'add_new_item' => __( 'Add Condition' ),
//       'new_item_name' => __( 'New Category Condition' ),
//       'menu_name' => __( 'Condition' ),
//     );
      
//     register_taxonomy('condition',array('product'), array(
//       'hierarchical' => true,
//       'labels' => $labels,
//       'show_ui' => true,
//       'show_admin_column' => true,
//       'query_var' => true,
//     ));
  
//   }

//   add_action( 'init', 'create_condition_taxonomy', 0 );

  //? ********** Register Berth product cat taxonomy **********
// function create_berth_taxonomy() {
    
//     $labels = array(
//       'name' => _x( 'Berth', 'taxonomy general name' ),
//       'singular_name' => _x( 'Berth', 'taxonomy singular name' ),
//       'search_items' =>  __( 'Search Berths' ),
//       'all_items' => __( 'All Berths' ),
//       'parent_item' => __( 'Parent Berth' ),
//       'parent_item_colon' => __( 'Parent Berth:' ),
//       'edit_item' => __( 'Edit Berth' ),
//       'update_item' => __( 'Update Berth' ),
//       'add_new_item' => __( 'Add Berth' ),
//       'new_item_name' => __( 'New Category Berth' ),
//       'menu_name' => __( 'Berth' ),
//     );
      
//     register_taxonomy('berth',array('product'), array(
//       'hierarchical' => true,
//       'labels' => $labels,
//       'show_ui' => true,
//       'show_admin_column' => true,
//       'query_var' => true,
//     ));
  
//   }
  
//   add_action( 'init', 'create_berth_taxonomy', 0 );

    //? ********** Register Tare product cat taxonomy **********
// function create_tare_taxonomy() {
    
//     $labels = array(
//       'name' => _x( 'Tare', 'taxonomy general name' ),
//       'singular_name' => _x( 'Tare', 'taxonomy singular name' ),
//       'search_items' =>  __( 'Search Tare' ),
//       'all_items' => __( 'All Tare' ),
//       'parent_item' => __( 'Parent Tare' ),
//       'parent_item_colon' => __( 'Parent Tare:' ),
//       'edit_item' => __( 'Edit Tare' ),
//       'update_item' => __( 'Update Tare' ),
//       'add_new_item' => __( 'Add Tare' ),
//       'new_item_name' => __( 'New Category Tare' ),
//       'menu_name' => __( 'Tare' ),
//     );
      
//     register_taxonomy('tare',array('product'), array(
//       'hierarchical' => true,
//       'labels' => $labels,
//       'show_ui' => true,
//       'show_admin_column' => true,
//       'query_var' => true,
//     ));
  
//   }
  
//   add_action( 'init', 'create_tare_taxonomy', 0 );

      //? ********** Register ATM product cat taxonomy **********
// function create_atm_taxonomy() {
    
//     $labels = array(
//       'name' => _x( 'ATM', 'taxonomy general name' ),
//       'singular_name' => _x( 'ATM', 'taxonomy singular name' ),
//       'search_items' =>  __( 'Search ATM' ),
//       'all_items' => __( 'All ATM' ),
//       'parent_item' => __( 'Parent ATM' ),
//       'parent_item_colon' => __( 'Parent ATM:' ),
//       'edit_item' => __( 'Edit ATM' ),
//       'update_item' => __( 'Update ATM' ),
//       'add_new_item' => __( 'Add ATM' ),
//       'new_item_name' => __( 'New Category ATM' ),
//       'menu_name' => __( 'ATM' ),
//     );
      
//     register_taxonomy('atm',array('product'), array(
//       'hierarchical' => true,
//       'labels' => $labels,
//       'show_ui' => true,
//       'show_admin_column' => true,
//       'query_var' => true,
//     ));
  
//   }
  
//   add_action( 'init', 'create_atm_taxonomy', 0 );

        //? ********** Register Ball product cat taxonomy **********
// function create_ball_taxonomy() {
    
//     $labels = array(
//       'name' => _x( 'Ball', 'taxonomy general name' ),
//       'singular_name' => _x( 'Ball', 'taxonomy singular name' ),
//       'search_items' =>  __( 'Search Ball' ),
//       'all_items' => __( 'All Ball' ),
//       'parent_item' => __( 'Parent Ball' ),
//       'parent_item_colon' => __( 'Parent Ball:' ),
//       'edit_item' => __( 'Edit Ball' ),
//       'update_item' => __( 'Update Ball' ),
//       'add_new_item' => __( 'Add Ball' ),
//       'new_item_name' => __( 'New Category Ball' ),
//       'menu_name' => __( 'Ball' ),
//     );
      
//     register_taxonomy('ball',array('product'), array(
//       'hierarchical' => true,
//       'labels' => $labels,
//       'show_ui' => true,
//       'show_admin_column' => true,
//       'query_var' => true,
//     ));
  
//   }
  
//   add_action( 'init', 'create_ball_taxonomy', 0 );

          //? ********** Register Length product cat taxonomy **********
// function create_length_taxonomy() {
    
//     $labels = array(
//       'name' => _x( 'Length', 'taxonomy general name' ),
//       'singular_name' => _x( 'Length', 'taxonomy singular name' ),
//       'search_items' =>  __( 'Search Length' ),
//       'all_items' => __( 'All Length' ),
//       'parent_item' => __( 'Parent Length' ),
//       'parent_item_colon' => __( 'Parent Length:' ),
//       'edit_item' => __( 'Edit Length' ),
//       'update_item' => __( 'Update Length' ),
//       'add_new_item' => __( 'Add Length' ),
//       'new_item_name' => __( 'New Category Length' ),
//       'menu_name' => __( 'Length' ),
//     );
      
//     register_taxonomy('Length',array('product'), array(
//       'hierarchical' => true,
//       'labels' => $labels,
//       'show_ui' => true,
//       'show_admin_column' => true,
//       'query_var' => true,
//     ));
  
//   }
  
  add_action( 'init', 'create_length_taxonomy', 0 );

//?  archive-product.php
//* Add gallery thumbs to woocommerce shop page
add_action('woocommerce_shop_loop_item_title','wps_add_extra_product_thumbs', 5);
function wps_add_extra_product_thumbs() {
	if ( is_shop() ) {
		global $product;
		$attachment_ids = $product->get_gallery_attachment_ids();
		echo '<div class="row">';
		foreach( array_slice( $attachment_ids, 0,3 ) as $attachment_id ) {
            ?>
<div class="col-4">
    <?php
		  	$thumbnail_url = wp_get_attachment_image_src( $attachment_id, 'thumbnail' )[0];
		  	echo '<img class="thumb" src="' . $thumbnail_url . '">';?> </div>
<?php 
		}
		echo '</div>';

	}
 }

 //* GET STOCKLIST: Condition, Specs, Price, Button, Stratton
add_action('woocommerce_after_shop_loop_item_title','stocklist_listings', 6);
function stocklist_listings() {
	if ( is_shop() ) {
		global $product;
?>
<div class="stock-condition"><?php echo the_field('condition'); ?></div>
<div class="specs-wrapper">
    <div class="row">
        <div class="col">
            <i class="fal fa-weight"></i>
            <p>Tare <br>
                <span class="font-weight-bold"><?php echo the_field('tare'); ?>kg</span>
            </p>
        </div>
        <div class="col">
            <i class="fal fa-balance-scale"></i>
            <p>Ball <br>
                <span class="font-weight-bold"><?php echo the_field('ball'); ?>kg</span>
            </p>
        </div>
        <div class="col">
            <i class="fal fa-weight-hanging"></i>
            <p>ATM <br>
                <span class="font-weight-bold"><?php echo the_field('atm'); ?>kg</span>
            </p>
        </div>
        <div class="col">
            <i class="fal fa-ruler"></i>
            <p> Length <br>
                <span class="font-weight-bold"><?php echo the_field('length'); ?>mm</span>
            </p>
        </div>
        <div class="col">
            <i class="fal fa-users"></i>
            <p>Berth <br>
                <span class="font-weight-bold"><?php echo the_field('berth'); ?></span>
            </p>
        </div>
    </div>
</div>

<div class="price-wrapper">
    <div class="row">
        <div class="col-6">
            <?php if($product->sale_price != '') { ?>
            <h5>
                <span class="text-uppercase">now</span>
                $<?php echo $product->sale_price; ?>
            </h5>
            <h6>
                <span class="text-uppercase">was</span>
                $<?php echo $product->regular_price; ?>
            </h6>
            <?php } else { ?>
            <h5>
                $<?php echo $product->regular_price; ?>
            </h5>
            <?php
                                          } ?>
        </div>
        <div class="col-6">
            <div class="btn">More Details</div>
        </div>
    </div>
</div>

<!-- Stratton Finance -->
<!-- <div class="stratton-finance-wrapper">
    <div class="row">
        <div class="col-4">
            Stratton Finance
        </div>
        <div class="col-8">
            Finance Option Availible Yours from <span>$200</span> per week
        </div>
    </div>
</div> -->
<!-- Stratton Finance -->
<?php	}

 }

//?  content-singe-product.php
  //* Display (Category), Make, Model, Condition, 
  add_action('woocommerce_product_meta_end','wps_add_mode_condition', 50);
  function wps_add_mode_condition() {
          global $product;?>
<table>
    <tbody>
        <tr>
            <td>
                Make
            </td>
            <td>
                <?php $terms=get_the_terms( $post->ID , 'make' );
                                foreach ( $terms as $term ) {
                                echo $term->name;}?>
            </td>
        </tr>
        <tr>
            <td>
                Model
            </td>
            <td>
                <?php $terms=get_the_terms( $post->ID , 'model' );
                                foreach ( $terms as $term ) {
                                echo $term->name;}?>
            </td>
        </tr>
        <tr>
            <td>
                Body
            </td>
            <td>
                <?php $terms=get_the_terms( $post->ID , 'body' );
                                foreach ( $terms as $term ) {
                                echo $term->name;}?>
            </td>
        </tr>
        <tr>
            <td>
                Condition
            </td>
            <td>
            <?php echo the_field('condition')?>
            </td>
        </tr>
        <tr>
            <td>Year
            </td>
            <td>
            <?php echo the_field('year')?>
            </td>
        </tr>
        <tr>
            <td>
                Berth
            </td>
            <td>
            <?php echo the_field('berth')?>
            </td>
        </tr>
        <tr>
            <td>
                Tare
            </td>
            <td>
            <?php echo the_field('tare')?>
            </td>
        </tr>
        <tr>
            <td>
                ATM
            </td>
            <td>
            <?php echo the_field('atm')?>
            </td>
        </tr>
        <tr>
            <td>
                Ball
            </td>
            <td>
                <?php echo the_field('ball')?>
            </td>
        </tr>
        <tr>
            <td>Length
            </td>
            <td>
            <?php echo the_field('length')?>
            </td>
        </tr>
    </tbody>
</table>

<?php }

  //* Deposit
  add_action( 'woocommerce_single_product_summary', 'add_text_after_excerpt_single_product', 25 );
  function add_text_after_excerpt_single_product(){
      global $product;
  ?>
<h5 class="text-center mt-5">Secure this caravan in seconds with a low deposit!</h5>
<?php }




  //* Add Specififcations Tab
  add_filter( 'woocommerce_product_tabs', 'specs_tab' );
 
  function specs_tab( $tabs ) {
   
      $tabs['specs_tab'] = array(
          'title'    => 'Specifications',
          'callback' => 'specs_tab_content',
          'priority' => 40,
      );
   
      return $tabs;
   
  }
   
  function specs_tab_content( $slug, $tab ) {
   
      echo the_field('specifications'); 
  }

    //* Add Floorplan Tab
    add_filter( 'woocommerce_product_tabs', 'floorplan_tab' );
    function floorplan_tab( $tabs ) {
        $tabs['floorplan_tab'] = array(
            'title'    => 'Floorplan',
            'callback' => 'floorplan_tab_content',
            'priority' => 50,
        );
        return $tabs;
    }
    function floorplan_tab_content( $slug, $tab ) {
        ?>
<div class="row text-center">
    <div class="col-12">
        <h2 class="text-center">Floorplan</h2>
        <img src="<?php the_field('floorplan_img'); ?>" />
    </div>
</div>
<?php
    }

        //* Add Video Tab
        add_filter( 'woocommerce_product_tabs', 'video_tab' );
        function video_tab( $tabs ) {
            $tabs['video_tab'] = array(
                'title'    => 'Video',
                'callback' => 'video_tab_content',
                'priority' => 60,
            );
            return $tabs;
        }
        function video_tab_content( $slug, $tab ) {
            ?>
<div class="row text-center">
    <div class="col-12">
        <h2 class="text-center">Video</h2>
        <?php the_field('video'); ?>
    </div>
</div>
<?php
        }
    

  //* Add Brochure Tab
  add_filter( 'woocommerce_product_tabs', 'brochure_tab' );
  function brochure_tab( $tabs ) {
      $tabs['brochure_tab'] = array(
          'title'    => 'Brochure',
          'callback' => 'brochure_tab_content',
          'priority' => 70,
      );
      return $tabs;
  }
  function brochure_tab_content( $slug, $tab ) {
      echo the_field('brochure_content'); ?>
<div class="row text-center">
    <div class="col-12">
        <a target="_blank" href='<?php echo the_field('brochure_link'); ?>'>
            <i class="fal fa-file-export fa-2x"></i>
        </a>
    </div>
</div> <?php
  }
  
    //* Add 3D Tour Tab
    add_filter( 'woocommerce_product_tabs', 'tour_tab' );
    function tour_tab( $tabs ) {
        $tabs['tour_tab'] = array(
            'title'    => '3D Tour',
            'callback' => 'tour_tab_content',
            'priority' => 80,
        );
        return $tabs;
    }
    function tour_tab_content( $slug, $tab ) { ?>
<div class="row text-center">
    <div class="col-12">
        <?php if( get_field('tour_link') ): ?>
        <h2 class="text-center">3D Tour</h2>
        <!-- Responsive iFrame -->
        <iframe width="853" height="480" src="<?php echo the_field('tour_link'); ?>" frameborder="0" allow="vr"
            allowfullscreen mozallowfullscreen webkitallowfullscreen></iframe>
        <!-- Responsive iFrame -->
        <?php else: ?>
        <h2 class="text-center">3D Tour not availible</h2>
        <?php endif; ?>
    </div>
</div>
<?php
    }

        //* Add Finance Tab
        add_filter( 'woocommerce_product_tabs', 'finance_tab' );
        function finance_tab( $tabs ) {
            $tabs['finance_tab'] = array(
                'title'    => 'Finance',
                'callback' => 'finance_tab_content',
                'priority' => 90,
            );
            return $tabs;
        }
        function finance_tab_content( $slug, $tab ) { ?>
<div class="row text-center">
    <div class="col-12">
    <h2 class="text-center">Finance Calculator</h2>
        <section class="finance" id="finance">
            <div class="finance__calculator">
                <div class="finance__calculator--finance">
                    <?php echo do_shortcode("[finance_calculator]"); ?>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
        }

                //* Add Trade In Tab
                add_filter( 'woocommerce_product_tabs', 'trade_tab' );
                function trade_tab( $tabs ) {
                    $tabs['trade_tab'] = array(
                        'title'    => 'Trade in Appraisals',
                        'callback' => 'trade_tab_content',
                        'priority' => 100,
                    );
                    return $tabs;
                }
                function trade_tab_content( $slug, $tab ) { ?>
<div class="row text-center">
    <div class="col-12">
    <h2 class="text-center">Trade in Appraisals</h2>
        <?php echo do_shortcode ('[ninja_form id=12]');?>
    </div>
</div>
<?php
                }


  //* Remove Addidional information tabs
    add_filter( 'woocommerce_product_tabs', 'remove_additional_information_tab' );
 
function remove_additional_information_tab( $tabs ) {
 
	unset( $tabs['additional_information'] );
	return $tabs;
 
}
