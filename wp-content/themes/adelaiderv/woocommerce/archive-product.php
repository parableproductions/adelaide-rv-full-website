<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */
defined( 'ABSPATH' ) || exit;
get_header( 'shop' );
/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );
?>

<section class="stocklist" id="stocklist">
    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( wc_get_page_id( 'shop' ) ), 'full' );?>
    <div class="top-header" id="top-header"
        style="background: url('<?php echo $thumb['0']; ?>') no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>


    <!-- INTRO -->
    <header class="woocommerce-products-header">
        <div class="stocklist__introduction">

            <div class="container">
                <!-- PAGE DESCRIPTION -->
                <?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' ); ?>
                <!-- PAGE DESCRIPTION -->
            </div>
        </div>
    </header>
    <!-- /INTRO -->



    <!-- WOOCOMMERCE LISTINGS START -->
    <div class="stocklist__listings">
        <div class="container">

            <div class="row">
                <!-- FIlTER -->
                <div class="col-md-12 col-lg-3">
                    <div class="stocklist__listings--filter">
                    	<div class="stocklist-title">
                    		<h2>Search by Filter</h2>
                    	</div>
						<?php echo do_shortcode('[searchandfilter id="1322"]') ?>
						<!-- Custom Reset btn -->
						<!-- <a href="<?php echo get_site_url(); ?>/stocklist/" class="btn-reset">Reset</a> -->
                    </div>
                </div>
                <!-- /FILTER -->

                <div class="col-md-12 col-lg-9">
                    <div class="stocklist__listings--stock">

                        <!-- START LISTINGS -->
                        <?php if ( woocommerce_product_loop() ) {
						/**
						 * Hook: woocommerce_before_shop_loop.
						 *
						 * @hooked woocommerce_output_all_notices - 10
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
						remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

						do_action( 'woocommerce_before_shop_loop' );
						woocommerce_product_loop_start(); ?>

                        <?php
						if ( wc_get_loop_prop( 'total' ) ) {
							while ( have_posts() ) {
								the_post();
								/**
								 * Hook: woocommerce_shop_loop.
								 */

								do_action( 'woocommerce_shop_loop' );

								wc_get_template_part( 'content', 'product' );
							}
						}
						woocommerce_product_loop_end(); ?>

                        <?php
						/**
						 * Hook: woocommerce_after_shop_loop.
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
						?>
                        <!-- END LISTISNG-->

                        <?php } else {
						/**
						 * Hook: woocommerce_no_products_found.
						 *
						 * @hooked wc_no_products_found - 10
						 */
						do_action( 'woocommerce_no_products_found' );
					}

					/**
					 * Hook: woocommerce_after_main_content.
					 *
					 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
					 */
					do_action( 'woocommerce_after_main_content' );
					?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer( 'shop' );