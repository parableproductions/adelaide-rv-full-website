<?php
// Template Name: Thank You
the_post();
get_header(); ?>

<section class="thank-you" id="thank-you">
<div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>	<div class="container">
		<div class="thank-you__wrap">
			<div class="thank-you__wrap--title">
				<h1><?php the_field('thank_you_title');?></h1>
			</div>
			<div class="thank-you__wrap--video">
				<iframe width="560" height="315" src="<?php the_field('thank_you_video');?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="thank-you__wrap--message">
				<?php the_content();?>
			</div>
			<div class="thank-you__wrap--back-button">
				<a href="/">Go Back</a>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>