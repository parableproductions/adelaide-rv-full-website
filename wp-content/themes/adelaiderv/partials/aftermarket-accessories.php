<?php
// Template Name: Aftermarket Accessories
the_post();
get_header(); ?>

<section class="aftermarket" id="aftermarket">
    <div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>
    <div class="container">
        <div class="aftermarket-copy">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>

        <div class="aftermarket-cards" id="aftermarket-cards">
            <div class="row">
                <?php
if( have_rows('aftermarket_accessories') ):
    while( have_rows('aftermarket_accessories') ) : the_row();?>
                <div class="col-xl-6 col-lg-12">
                    <div class="aftermarket-cards_wrapper">
                        <div class="card">
                            <div class="img-wrapper">
                                <a href="#"><img class="card-img-top" src="<?php the_sub_field('image');?>"></a>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title"><?php the_sub_field('title');?></h5>
                                <p class="card-text py-3"><?php the_sub_field('intro');?></p>
                                <a class="btn btn-primary text-uppercase" href="#" data-toggle="modal"
                                    data-src="<?php the_sub_field('video');?>" data-target="#accessorieModal<?php the_sub_field('id');?>">Learn
                                    More</a>
                                <a class="btn btn-primary text-uppercase" href="#" data-toggle="modal"
                                    data-src="<?php the_sub_field('video');?>" data-target="#getquoteModal<?php the_sub_field('id');?>"><?php the_sub_field('button_text');?></a>
                            </div>
                        </div>
                    </div>
                </div>

                        <!-- Modal Accessories -->
        <div class="modal fade" id="accessorieModal<?php the_sub_field('id');?>">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    <div class="modal-body">
                        <h3 class="card-title text-center"><?php the_sub_field('title');?></h3>
                        <?php the_sub_field('content');?>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Modal -->

        <!-- Modal Get Quote -->
        <div class="modal fade" id="getquoteModal<?php the_sub_field('id');?>">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                    <div class="modal-body">
                        <h2 class="card-title mb-5 text-center"><?php the_sub_field('button_text');?></h2>
                        <?php the_sub_field('button_shortcode');?>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Modal -->
                <?php endwhile;
                    endif; ?>
            </div>
        </div>
    </div>

</section>

<?php get_footer(); ?>