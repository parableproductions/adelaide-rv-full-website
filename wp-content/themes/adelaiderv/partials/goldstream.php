<?php
// Template Name: Goldstream
the_post();
get_header(); ?>

<section class="brands" id="brands">
    <div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>

    <div class="container">
        <div class="brands-copy">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php endwhile; ?>
            <?php endif; ?>
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>

        <div class="brands-cards" id="brands-cards">
            <div class="row">
                <?php
$args = array(
    'post_type' => 'range',
    'tax_query' => array(
        array(
            'taxonomy' => 'manufacturer_category',
            'field'    => 'slug',
            'terms'    => 'Goldstream RV',
        ),
    ),
);
$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) { 
    while ( $the_query->have_posts() ) {
        $the_query->the_post();?>
                <div class="col-md-6">
                    <div class="brands-cards_wrapper">
                        <div class="card text-center">
                            <div class="img-wrapper">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail();?></a>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title"><?php the_title();?></h5>
                                <p class="card-text py-3"><?php echo wp_trim_words( get_field('intro'), 40, '...' );?></p>
                                <a href="<?php the_permalink(); ?>" class="btn btn-primary text-uppercase">More
                                    Details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
    }} 
wp_reset_postdata();?>
            </div>
        </div>
    </div>
</section>




<?php get_footer(); ?>