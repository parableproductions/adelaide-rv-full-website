<?php
// Template Name: Videos & Tutorials
the_post();
get_header(); ?>


<section class="video-tutorials" id="video-tutorials">
<div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>    <div class="container">
        <div class="video-tutorials__introduction">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>

        <!-- Filter -->
        <div class="video-tutorials__navigation">
            <h3>Fillter by</h3>
            <div class="row">
                <div class="col-md-4 pad-b">
                    <a class="js-filter-item" href="<?php home_url('videos-tutorials');?>">All</a>
                </div>
                <?php
                     $cat_args = array( 
                         'type' => 'videos', 
                         'taxonomy' => 'video_category', 
                    );

                    $categories = get_categories($cat_args);
                    foreach($categories as $cat) : ?>
                <div class="col-md-4 pad-b">

                    <a class="js-filter-item" data-category="<?php echo $cat->term_id;?>"
                        href="<?php echo get_category_link($cat->term_id);?>"> <?php echo $cat->name;?> </a>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
        <!-- Filter -->

        <!-- Videos -->
        <div class="js-filter">
            <div class="video-tutorials__video">
                <div class="row">

                    <?php $args = array (
                        'post_type' => 'videos',
                        'posts_per_page' => -1
                    );

                    $query = new WP_Query($args);
                    if($query->have_posts()):
                        while($query->have_posts()) : $query->the_post(); ?>
                    <?php $videoid = get_the_ID(); ?>

                                    <div class="col-sm-6 col-md-3">
                                        <div class="video-tutorials__video--list">
                                            <a href="#" data-toggle="modal" data-target="#videoModal<?php echo $videoid; ?>">
                                                <div class="list-wrapper">
                                                    <?php if ( has_post_thumbnail()) : ?>
                                                    <?php the_post_thumbnail(); ?>
                                                    <?php endif; ?>
                                                    <div class="videos-overlay"></div>
                                                    <!-- <img class="videos-logo" src="<?php lp_image_dir(); ?>/adelaide-rv-logo_white.png">-->
                                                    <h5 class="videos-title"><?php the_title();?></h5>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- Modal -->
                                    <div class="modal fade" id="videoModal<?php echo $videoid; ?>">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <iframe id="iframeModal" width="850" height="450" src="<?php the_field('single_video'); ?>?autoplay=0"
                                                        frameborder="0" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                    <?php endwhile;
                    endif;
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>

        <!-- Videos -->
    </div>
</section>



<?php get_footer(); ?>