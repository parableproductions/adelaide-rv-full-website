<?php
// Template Name: Happy Customer Wall
the_post();
get_header();

$images = get_field('gallery')
?>


<section class="customer" id="customer">
    <div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>
    <div class="container">
        <div class="customer-copy">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php endwhile; ?>
            <?php endif; ?>
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>

        <div class="customer-wall" id="customer-wall">

             <?php if($images):?>
                <div class="customer-wall__wrap">
                    <?php foreach($images as $image):?>
                        <a href="<?php echo $image['sizes']['large'];?>"  data-lightbox="mygallery">
                            <img src="<?php echo $image['sizes']['large'];?>">
                        </a>
                    <?php endforeach;?>
                </div>
            <?php endif;?>




            <!--<div class="row">
                <?php
                if( have_rows('happy_customer_wall') ):
                    while( have_rows('happy_customer_wall') ) : the_row();?>
                <div class="col-md-4">
                    <div class="customer-wall_wrapper">
                        <div class="img-wrapper">
                            <img src="<?php the_sub_field('customer_images');?>">
                        </div>
                    </div>
                </div>
                <?php endwhile;
                    endif; ?>
            </div>-->
        </div>
    </div>
</section>



<?php get_footer(); ?>