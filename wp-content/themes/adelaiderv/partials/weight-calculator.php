<?php
// Template Name: Weight Calculator
the_post();
get_header(); ?>

<section class="finance" id="finance">
<div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>
     <div class="finance__calculator">
     	 <div class="container">
     	 	<div class="top">
     	 		 <h1><?php the_title();?></h1>
                   <?php the_content();?>

     	 	</div>
     	 	<div class="finance__calculator--loan">
     	 		<!-- <div class="title">
     	 			<h3>Loan Calculator</h3>
     	 		</div> -->
     	 		<?php echo do_shortcode('[CP_CALCULATED_FIELDS id="7"]'); ?>
     	 	</div>
	    </div>
     </div>
</section>
<?php get_footer(); ?>