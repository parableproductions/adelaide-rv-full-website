<?php
// Template Name: Brands
the_post();
get_header(); ?>


<section class="brands" id="brands">
    <div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>
    <div class="container">
        <div class="brands-copy">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>

        <div class="brands-cards" id="brands-cards">
            <div class="row">
                <?php
if( have_rows('all_brands') ):
    while( have_rows('all_brands') ) : the_row();?>
                <div class="col-md-6">
                    <div class="brands-cards_wrapper">
                        <div class="card">
                            <div class="img-wrapper">
                                <a href="<?php home_url();?><?php the_sub_field('link');?>"><img class="card-img-top" src="<?php the_sub_field('feature_image');?>"></a>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title"><?php the_sub_field('title');?></h5>
                                <p class="card-text py-3"><?php the_sub_field('intro');?></p>
                                <a href="<?php home_url();?><?php the_sub_field('link');?>"
                                    class="btn btn-primary text-uppercase">More Details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile;
                    endif; ?>
            </div>
        </div>

    </div>
</section>





<?php get_footer(); ?>