<?php
// Template Name: About
the_post();
get_header(); ?>

<section class="about-responsive">
<div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>

        <div class="about-responsive__top">
            <div class="container">
                <div class="about-responsive__top--copy">
                    <h1><?php the_title();?></h1>
                    <?php the_content();?>
                                   </div>
            </div>
        </div>

        <div class="about-responsive__topbody">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <div class="about-responsive__topbody--copy">
                            <?php the_field('top_content');?>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="about-responsive__topbody--img">
                            <img src="<?php the_field('top_image');?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="quote">
             <div class="container">
                <div class="quote__wrapper">
                    <div class="quote__wrapper--text">
                        <?php the_field('quote');?>
                        <h6 class="mt-4"><?php the_field('quote_by');?></h6>
                    </div>
                </div>
            </div>
        </div>

        <div class="about-responsive__topbody">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 order-md-2 col-md-6">
                        <div class="about-responsive__topbody--copy">
                        <?php the_field('bottom_content');?>

                        </div>
                    </div>
                    <div class="col-sm-12 order-md-1 col-md-6">
                        <div class="about-responsive__topbody--img">
                            <img src="<?php the_field('bottom_image');?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="about-responsive__team" id="our-team">
            <div class="container">
                <div class="about-responsive__team--title">
                     <h1>Our Team</h1>
                </div>
                <div class="row">
                <?php
if( have_rows('our_team') ):
    while( have_rows('our_team') ) : the_row(); ?>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="about-responsive__team--wrap">
                        <a href="mailto:<?php the_sub_field('email');?>?&cc=<?php the_sub_field('cc_email');?>">
                                <img class="team-image" src="<?php the_sub_field('image');?>">
                                <div class="team-copy">
                                    <h4><?php the_sub_field('name');?></h4>
                                    <p><?php the_sub_field('job_title');?></p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php   endwhile;
endif; ?>
                </div>
            </div>
        </div>

</section>

<?php get_footer(); ?>