<?php
// Template Name: Help & Tips
the_post();
get_header(); ?>

<section class="helptips" id="helptips">
    <div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;"></div>
        
    <div class="container">
        <div class="helptips__introduction">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php endwhile; ?>
            <?php endif; ?>
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
        </div>

        <!-- FILTER -->
        <div class="helptips__navigation">
              <h3>Fillter by</h3>
            <div class="row">
                <div class="col-md-4 pad-b">
                    <a class="js-filter-item2" href="<?php home_url('help-tips');?>">All</a>
                </div>
                <?php 
 $cat_args = array( 
     'type' => 'articles', 
     'taxonomy' => 'articles_category', 
);

$categories = get_categories($cat_args);
foreach($categories as $cat) : ?>
                <div class="col-md-4 pad-b">
                    <a class="js-filter-item2" data-category="<?php echo $cat->term_id;?>"
                        href="<?php echo get_category_link($cat->term_id);?>"> <?php echo $cat->name;?> </a>
                </div>
                <?php endforeach; ?>
                <?php wp_reset_query(); ?>
            </div>
        </div>
        <!-- /FILTER -->

        <!-- Articles -->
        <div class="js-filter2">

            <div class="helptips__content">
                <div class="row">
                <?php $args = array ( 
            'post_type' => 'articles', 
            'posts_per_page' => -1
        );
$query = new WP_Query($args);

if($query->have_posts()): 
    while($query->have_posts()) : $query->the_post(); ?>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="helptips__content--wrap">
                            <div class="top-image">
                                <div class="overlay-bg"></div>
                                <?php if ( has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail(); ?>
                                <?php endif; ?>
                                <!-- <img class="help-img" src="<?php lp_image_dir(); ?>/video.jpg"> -->
                                <div class="published-date"><?php echo get_the_date(); ?></div>
                            </div>
                            <div class="infor-section">
                                <h5 class="help-title"><?php the_title();?></h5>

                                <p> <?php echo wp_trim_words( get_field('articles_content'), 20, '...'); ?></p>
                                <a href="<?php the_permalink(); ?>" target="_blank"> More Details</a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- /Articles -->

    </div>
</section>






<?php get_footer(); ?>