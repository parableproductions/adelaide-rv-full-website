<?php
// Template Name: Servicing, Maintenance & Repairs
the_post();
get_header(); ?>


<div class="top-header" id="top-header"
    style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
    <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
</div>

<section class="services" id="services">
        <div class="container">
            <div class="services__top">
                 <h1><?php the_title(); ?></h1>
                 <?php the_content(); ?>
                
            </div>
        </div>

        <div class="services__price">
            <div class="container">
                <div class="services__price--title">
                    <h1>Our Pricing List</h1>
                </div>
                <div class="services__price--body">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <div class="content-wrap">
                                <div class="content-wrap__image">
                                    <i class="price-icon <?php the_field('first_service_icon');?>"></i>
                                </div>
                                <div class="content-wrap__copy">
                                    <h3>First Service</h3>
                                    <h4>Single Axle from:<span><?php the_field('first_service_single_axle');?></span></h4>
                                    <h4>Tandem Axle from:<span><?php the_field('first_service_tandem_axle');?></span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="content-wrap">
                                <div class="content-wrap__image">
                                    <i class="price-icon <?php the_field('standard_yearly_icon');?>"></i>
                                </div>
                                <div class="content-wrap__copy">
                                    <h3>Standard Yearly Servicing</h3>
                                    <h4>Single Axle from:<span><?php the_field('standard_yearly_single_axle');?></span></h4>
                                    <h4>Tandem Axle from:<span><?php the_field('standard_yearly_tandem_axle');?></span></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="content-wrap">
                                <div class="content-wrap__image">
                                    <i class="price-icon <?php the_field('premium_yearly_icon');?>"></i>
                                </div>
                                <div class="content-wrap__copy">
                                    <h3>Premium Yearly Servicing</h3>
                                    <h4>Single Axle from:<span><?php the_field('premium_yearly_single_axle');?></span></h4>
                                    <h4>Tandem Axle from:<span><?php the_field('premium_yearly_tandem_axle');?></span></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="services__type">
            <div class="container">
                <div class="services__type--title">
                    <h1><?php the_field('caravan_servicing_title')?></h1>
                </div>
                <div class="services__type--body">
                    <p><?php the_field('caravan_servicing_intro')?></p>
                    <?php
if( have_rows('services_repeater') ): ?>
                    <ul>
                    <?php while( have_rows('services_repeater') ) : the_row(); ?>
                        <li><i class="fas fa-check-square"></i><?php the_sub_field('services');?></li>
                    <?php endwhile;?>
                    </ul>
                <?php endif; ?>
                </div>
            </div>
        </div>


        <div class="services__form">
            <div class="container">
                <div class="services__form--title">
                <h1><?php the_field('form_title');?></h1>
                <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>-->
                </div>
                <div class="services__form--body">
                    <?php echo do_shortcode ('[ninja_form id=6]');?>
                </div>
            </div>
        </div>
        

        <div class="services__faq">
            <div class="container">
                <div class="services__faq--title">
                <h1>Frequently Asked Questions</h1>
                <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>-->
                </div>

                <div class="services__faq--body">
                    <div id="services_question1" class="question"><p>Q. When is the first service for a new caravan required?</p><i class="fas fa-angle-down"></i></div>
                    <div id="services_answer1" class="answer hide"><p>A caravans first service is due 3 months or 1000KM which ever comes first</p></div>

                    <div id="services_question2" class="question"><p>Q. What kind of service is involved for a Caravan that hasn't been used for a while and is about to go on a trip?</p><i class="fas fa-angle-down"></i></div>
                    <div id="services_answer2" class="answer hide"><p>
                        Check tyre pressures<br/>
                        Check wheel bearings and repack<br/>
                        Check brakes (including handbrake) and adjust if needed<br/>
                        Check all lights on caravan<br/> Check chassis for faults<br/>
                        Check gas components and 240v components<br/>
                        Check house batteries<br/>
                        Check inside lights<br/>
                        Check Hot water System<br/>
                        Plus silicone checks and water test</p></div>


                    <div id="services_question3" class="question"><p>Q. What sort of service does a Caravan with high mileage usually go through? </p><i class="fas fa-angle-down"></i></div>
                    <div id="services_answer3" class="answer hide"><p>
                        Check tyre pressures<br/>
                        Check wheel bearings and repack<br/>
                        Check brakes (including handbrake) and adjust if needed<br/>
                        Check all lights on caravan<br/>
                        Check chassis for faults<br/>
                        Check gas components and 240v components<br/>
                        Check house batteries<br/>
                        Check inside lights<br/>
                        Check Hot water System<br/>
                        Plus silicone checks and water test</p></div>


                    <div id="services_question4" class="question"><p>Q. How often should I take my caravan in for a service? </p><i class="fas fa-angle-down"></i></div>
                    <div id="services_answer4" class="answer hide"><p>It is recommended to have your caravan serviced every 10,000km or 12 months whichever comes first.</p></div>

                    <div id="services_question5" class="question"><p>Q. What do i need to keep an eye when on the road? </p><i class="fas fa-angle-down"></i></div>
                    <div id="services_answer5" class="answer hide"><p>
                        Most critical parts of the caravan should be subject to a daily visual check. This includes coupling, lights, tyres, this will keep you safe on the road. Should you notice anything abnormal please call the Green RV Servicing department<br/><br/>

                        Checking wheels nuts (with a wheel brace), tyre pressures (with a gauge) can be less frequent but must be regular to ensure your safety.<br/><br/>

                        When towing it is good practice to stop after the first 20 to 30 km and just do a quick check again of the important bits to make sure everything is OK.

                    </p></div>

                </div>
            </div>
        </div>

        <div class="services__location">
            <div class="container">
                <div class="services__location--title">
                <h1>Our Service Location</h1>
                <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>-->
                </div>

                <div class="services__location--body">
                     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3276.6734225895566!2d138.59476431587927!3d-34.78899588041176!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ab0b727397b6f25%3A0x90418287dfb2c87f!2sAdelaide%20RV!5e0!3m2!1sen!2sau!4v1591314410980!5m2!1sen!2sau"width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0">
                         </iframe>
                </div>
            </div>
        </div>
</section>



<?php get_footer(); ?>