<?php
// Template Name: Contact
the_post();
get_header(); ?>


<div class="top-header" id="top-header"
    style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
    <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
</div>

<section class="contact" id="contact">
    <div class="contact-wrapper">
        <div class="contact-wrapper__form">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-8 form-top-up">
                        <div class="contact-form">
                            <div class="contact-copy">
                                <h1><?php the_title(); ?></h1>
                                <?php the_content(); ?>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 onthetop">
                        <div class="info-cards_wrapper">
                            <div class="card">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3276.6734225895566!2d138.59476431587927!3d-34.78899588041176!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ab0b727397b6f25%3A0x90418287dfb2c87f!2sAdelaide%20RV!5e0!3m2!1sen!2sau!4v1591314410980!5m2!1sen!2sau"
                                    width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""
                                    aria-hidden="false" tabindex="0">
                                </iframe>

                                <div class="card-body">
                                    <div class="address" id="address">
                                        <h5>WHERE TO FIND US</h5>
                                        <div class="address_wrapper">
                                            <p>
                                                <?php echo the_field('street', 'option'); ?><br>
                                                <?php echo the_field('suburb', 'option'); ?><br>
                                                <?php echo the_field('state', 'option'); ?>
                                                <?php echo the_field('postcode', 'option'); ?><br>
                                            </p>
                                        </div>
                                    </div>

                                    <div class="opening-hours" id="opening-hours">
                                        <h5>OPENING HOURS</h5>
                                        <div class="opening-hours_wrapper">
                                            <table class="table table-borderless">
                                                <tbody>
                                                    <?php if( have_rows('opening_hours', 'option') ): 
                                                    while( have_rows('opening_hours', 'option') ) : the_row(); ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo the_sub_field('day'); ?>
                                                        </td>
                                                        <td>
                                                            <?php echo the_sub_field('time'); ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                endwhile;
                                            endif;
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="contact-details" id="contact-details">
                                        <h5>CONTACT US</h5>
                                        <div class="contact-details_wrapper">
                                            <p>
                                                <span class="font-weight-bold">Email</span> <a
                                                    href="mailto:sales@adelaiderv.com.au"><?php echo the_field('email', 'option'); ?></a><br><br>
                                                <span class="font-weight-bold">Phone</span> <a
                                                    href="tel:0882818889"><?php echo the_field('phone', 'option'); ?></a><br>

                                            </p>
                                        </div>
                                    </div>

                                </div>

                                <div class="instagram card-footer">
                                    <a target="_blank" href="<?php echo the_field('instagram', 'option');?>" class="">
                                        <i class="fa fa-instagram fa-lg"></i>
                                        Follow us on Instagram
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?php get_footer(); ?>