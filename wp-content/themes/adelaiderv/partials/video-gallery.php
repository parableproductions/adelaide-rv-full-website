<?php
// Template Name: Videos Category
the_post();
get_header(); ?>

<section class="video-tutorials" id="video-tutorials">
<div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>    <div class="container">
        <div class="video-tutorials__video">
            <div class="video-tutorials__video--title">
                <h1>Video Gallery</h1>
            </div>
           <div class="video-tutorials__video--list">
                <?php echo do_shortcode ('[aiovg_categories]') ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>