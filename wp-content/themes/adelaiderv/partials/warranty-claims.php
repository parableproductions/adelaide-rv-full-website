<?php
// Template Name: Warranty Claims
the_post();
get_header(); ?>


<div class="top-header" id="top-header"
    style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
    <!--<img class="warranty-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
</div>

<section class="warranty" id="warranty">
   <div class="container">
            <div class="warranty__top">
                 <h1><?php the_title(); ?></h1>
                <?php the_content();?>
                
            </div>
        </div>

     <div class="warranty__form">
        <div class="container">
            <div class="warranty__form--title">
            <h1><?php the_field('form_title');?></h1>
            <!--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>-->
            </div>
            <div class="warranty__form--body">
                <?php echo do_shortcode ('[ninja_form id=9]');?>
            </div>
        </div>
    </div>
</section>



<?php get_footer(); ?>