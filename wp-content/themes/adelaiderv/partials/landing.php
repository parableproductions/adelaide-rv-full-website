<section class="landing" id="landing">
        <video id="videoBG" autoplay muted loop>
            <source src="<?php lp_image_dir(); ?>/intro-video.mp4" type="video/mp4">
            </source>
            <!--<source src="https://file-examples-com.github.io/uploads/2017/04/file_example_MP4_1920_18MG.mp4" type="video/mp4"></source>-->
        </video>

    <div class="container">
        <div class="form-wrapper">
            <div class="form-copy">
                <h2>Start your journey here</h2>
                <p>Search our stock</p>
                <div class="search-form">
                    <!--<?php echo do_shortcode ('[searchandfilter fields="manufacturer_category,model,search" submit_label="Shop now"]') ?>-->
                    <!--<?php echo do_shortcode ('[searchandfilter fields="make,model,body,condition,search" submit_label="Shop now"]') ?>-->
                    <?php echo do_shortcode ('[searchandfilter id="383"]') ?>

                    <!--<div class="search-form__brands">
                        <div class="sub-brand">
                             <a href="<?php echo get_home_url(); ?>/nova-caravans"><img class="logo" src="<?php lp_image_dir(); ?>/adelaide-rv-logo_white.png" width="180">
                             <p>Nova Caravans</p></a>
                        </div>
                        <div class="sub-brand">
                             <a href="<?php echo get_home_url(); ?>/supreme-caravans"><img class="logo" src="<?php lp_image_dir(); ?>/adelaide-rv-logo_white.png"  width="180">
                             <p>Supreme Caravans</p></a>
                        </div>
                        <div class="sub-brand">
                             <a href="<?php echo get_home_url(); ?>/goldstream-rv"><img class="logo" src="<?php lp_image_dir(); ?>/adelaide-rv-logo_white.png"  width="180">
                             <p>Goldstream RV</p></a>
                        </div>
                       <div class="sub-brand">
                             <a href="<?php echo get_home_url(); ?>/leader-caravans"><img class="logo" src="<?php lp_image_dir(); ?>/adelaide-rv-logo_white.png"  width="180">
                             <p>Leader Caravan</p></a>
                        </div>
                    </div>-->
                </div>
        </div>
    </div>
    <div class="overlay-black"></div>
    <!--<div class="arrow-down">
        <a href="#aboutadelaide-rv">Enter Here<br /><i class="fas fa-chevron-down"></i></a>
    </div>-->
</section>


<section class="aboutadelaide-rv" id="aboutadelaide-rv">
    <div class="container">
        <div class="aboutadelaide-rv__copy">
            <div class="main-title">
                <h1><?php the_title(); ?></h1>
            </div>
            <?php the_content(); ?>
        </div>
    </div>
</section>

<section class="statadelaide-rv" id="statadelaide-rv">
    <div class="container">
        <div class="main-title">
            <h2>Awards & Achievement</h2>
        </div>
        <div class="statadelaide-rv__body">
            <div class="statadelaide-rv__body--one">
                <div class="content">
                    <div class="content__image">
                        <i class="fas fa-award"></i>
                    </div>
                    <div class="content__copy">
                        <p><?php echo the_field('award_one'); ?></p>
                    </div>
                </div>
            </div>
            <div class="statadelaide-rv__body--one">
                <div class="content">
                    <div class="content__image">
                        <i class="fas fa-award"></i>
                    </div>
                    <div class="content__copy">
                        <p><?php echo the_field('award_two'); ?></p>
                    </div>
                </div>
            </div>
            <div class="statadelaide-rv__body--one">
                <div class="content">
                    <div class="content__image">
                        <i class="fas fa-trophy"></i>
                    </div>
                    <div class="content__copy">
                        <p><?php echo the_field('award_three'); ?></p>
                    </div>
                </div>
            </div>
           <div class="statadelaide-rv__body--one">
                <div class="content">
                    <div class="content__image">
                        <i class="fas fa-award"></i>
                    </div>
                    <div class="content__copy">
                        <p><?php echo the_field('award_four'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gallery-slider" id="gallery-slider">
    <div class="container">
        <div class="gallery-slider__wrapper">
            <div class="main-title">
                <h1>Our Brands</h1>
            </div>
            <div class="gallery-slider__wrapper--top">
                <?php
                if( have_rows('our_brands') ):
                    while( have_rows('our_brands') ) : the_row(); ?>
                                <div class="slider-wrap">
                                    <div class="row">
                                        <div class="top-image col-md-12 col-lg-6">
                                            <img src="<?php the_sub_field('image');?>" width="650">
                                        </div>
                                        <div class="col-md-12 col-lg-6">
                                            <div class="copy-wrap">
                                                <div class="copy-title">
                                                    <h2><?php the_sub_field('title')?></h2>
                                                </div>
                                                <?php the_sub_field('content');?>
                                                <a href="<?php home_url();?><?php the_sub_field('explore_the_range_link');?>" class="btn-white btn-primary text-uppercase">Explore the Range</a>
                                                <a href="<?php home_url();?><?php the_sub_field('whats_in_stock');?>" class="btn-white btn-primary text-uppercase">Whats in Stock</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php   endwhile;
                    endif; ?>
            </div>


            <div class="gallery-slider__wrapper--bottom">
                <?php
if( have_rows('our_brands') ):
    while( have_rows('our_brands') ) : the_row(); ?>
                <div class="slider-wrap">
                    <div class="slider-wrap--image">
                        <div class="overlay-black"></div>
                        <div class="image-title">
                            <h4><?php the_sub_field('title');?></h4>
                        </div>
                        <img src="<?php the_sub_field('image');?>">
                    </div>
                </div>
                <?php   endwhile;
endif; ?>
            </div>

        </div>
    </div>
</section>

<section class="testimonials" id="testimonials">
    <div class="container">
        <div class="testimonials__title">
            <h1>Testimonials</h1>
        </div>

        <div class="test-wraper">
            <?php echo do_shortcode ('[testimonial_rotator id=187]') ?>
        </div>
    </div>
</section>

<section class="map" id="map">
    <div class="map__wrapper">
        <!--<iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3276.6735936541386!2d138.59476431620803!3d-34.788991574833624!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzTCsDQ3JzIwLjQiUyAxMzjCsDM1JzQ5LjAiRQ!5e0!3m2!1sen!2sau!4v1587692880024!5m2!1sen!2sau"
            width="100%" height="650" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
            tabindex="0"></iframe>-->

            <iframe src="https://www.google.com/maps/d/embed?mid=1kLoT0yqMUGMNY5xbW9v0Nhpcz94Z0fQZ&hl=en" width="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </div>
</section>