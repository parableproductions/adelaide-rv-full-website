<?php
// Template Name: Reviews
the_post();
get_header(); ?>

<section class="review" id="review">
<div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>
    <div class="container">
        <div class="review-copy">
            <h1><?php the_title();?></h1>
            <?php the_content();?>
        </div>
    </div>
</section>

<?php get_footer(); ?>