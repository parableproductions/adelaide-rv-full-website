<?php
// Template Name: Finance Calculator
the_post();
get_header(); ?>

<section class="finance" id="finance">
<div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url(); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>
     <div class="finance__calculator">
     	 <div class="container">
     	 	<div class="top">
     	 		 <h1>Adelaide RV Finance</h1>

				<p>With interest rates at ridiculously low rates why wouldn’t it be the right time to buy? Adelaide RV work closely with the team at Stratton Finance to ensure that you receive the best possible repayment options, tailored to suit each individual’s needs.</p>

				<p>Financing both New and Used, with no hidden costs and $0 deposit options, Stratton Finance offer a no fuss service which will have you travelling in your dream Caravan sooner than you think.</p>
     	 	</div>
     	 	<div class="finance__calculator--loan">
     	 		<div class="title">
     	 			<h3>Loan Calculator</h3>
     	 		</div>
     	 		<?php echo do_shortcode("[loan_calculator]"); ?>
     	 	</div>
     	 	<div class="finance__calculator--finance">
     	 		<div class="title">
     	 			<h3>Finance Calculator</h3>
     	 		</div>
     	 		<?php echo do_shortcode("[finance_calculator]"); ?>
     	 	</div>
	    </div>
     </div>
</section>
<?php get_footer(); ?>