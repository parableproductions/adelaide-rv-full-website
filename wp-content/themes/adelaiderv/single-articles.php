<?php
// Template Name: Help & Tips Single
the_post();
get_header(); ?>

<?php 
$post_id = 147;
?>
<section class="help-info" id="help-info">
<div class="top-header" id="top-header"
        style="background: url(<?php echo get_the_post_thumbnail_url($post_id); ?>) no-repeat center/cover;">
        <!--<img class="contact-wrapper__header--image" src="<?php lp_image_dir(); ?>/lifestyle_02.jpg">-->
    </div>

    <div class="container">
        <div class="help-info__wrap">
            <div class="row">
                <div class="col-md-8">
                    <div class="help-info-content">
                        <div class="title"><h2><?php the_title();?></h2></div>
                        <!-- <img class="help-img" src="<?php lp_image_dir(); ?>/video.jpg"> -->
                        <div class="body-copy">
                          <?php the_field('articles_content');?>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="side-info-bar">
                      <?php echo do_shortcode ('[ninja_form id=2]') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>