<?php
function finance_output_style_one($apicallbackdata){
	//$terminyears = $apicallbackdata['termInMonths']/12;
	
	$output = '<div class="stratton-finance-container style-one">';
    $output .= '<div class="row">';
    
    $output .= '<div class="col-7">';
    $output .= '<div class="finavail">';
	$output .= 'Finance Available';
	$output .= '</div>';
    $output .= '<div class="price">';
	$output .= "<span class=\"from\">from</span> $". number_format($apicallbackdata['weeklyRepayments'],0, '.', '') ." <span class=\"time\">p/w</span>";
	$output .= '</div>';
	$output .= '<div class="estimateterms">';
	$output .= "<a class=\"disclaimer\" data-toggle=\"collapse\" href=\"#strattonDisclaimer\" role=\"button\" aria-expanded=\"false\" aria-controls=\"strattonDisclaimer\">^ Estimate Details</a>";
	$output .= '</div>';
    $output .= '</div>';
    
    $output .= '<div class="col-5">';
    $output .= '<div class="logo">';
	$output .= '<img class="stratton-logo img-fluid" src="'.plugins_url( '/stratton-finance/img/Stratton-Finance-inverted.png').'">';
	$output .= '</div>';
    $output .= '</div>';
    
    $output .= '</div>';
    
    $output .= '<div class="row">';
    $output .= '<div class="col-12">';
    $output .= '<a class="btn btn-quote btn-block" id="strattonGetQuote" onclick="window.open(\'https://www.strattonfinance.com.au/online-quote/boat-caravan-rv-finance?rcid='.$apicallbackdata['StrattonRCID'].'&typeDDText='.$apicallbackdata['assetRadioBtn']."&amount=".$apicallbackdata['inputPrice']."&utm_source=".$apicallbackdata['StrattonUTMSource'].'&utm_medium=Integrated&utm_campaign='.$apicallbackdata['StrattonUTMCampaign']."&utm_content=Live_quote','Stratton Finance - Get a Quote','width=1024,height=800')\">";
    $output .= 'Get A Quote';
    $output .= '</a>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= "<div class=\"collapse strattonDisclaimer\" id=\"strattonDisclaimer\">";
	$output .= "<div class=\"card card-body\">";
	$output .= "^This weekly repayment estimate is provided by Stratton Finance Pty Ltd (Australian Credit Licence No. 364340) (\"Stratton\"). Stratton is a finance broker. This repayment is calculated with an interest rate of ".$apicallbackdata['interestRate']."% p.a. over a term of ". $apicallbackdata['termInMonths']." months with a ". $apicallbackdata['residualPercentage']."% residual / balloon payment. Other residual / balloon amounts are available, including the option of no residual / balloon. A lower residual / balloon will result in higher repayments. The interest rate is indicative of the rates on offer through Stratton's lending panel. The repayment estimate applies to the price shown. The price shown may not include other additional costs such as stamp duty, government fees and other charges payable in relation to the item. This estimate should be used for information purposes only and is not an offer of finance on particular terms. Credit fees, service fees and charges may apply. Credit to approved applicants only. A quote, details of all fees and charges may be obtained by contacting Stratton via <a href=\"https://www.strattonfinance.com.au/\" target=\"_blank\">stratton.com.au</a> or calling <a href=\"tel:1300955600\">1300 STRATTON</a> (<a href=\"tel:1300955600\">1300 955 600</a>).";
	$output .= "</div>";
	$output .= "</div>";
	$output .= '</div>';
	
	return $output;
}
