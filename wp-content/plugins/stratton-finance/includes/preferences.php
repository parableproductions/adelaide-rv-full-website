<?php
if (!function_exists('stratton_finance_config')){

	add_action( 'admin_init' , 'stratton_finance_register_config' );
	function stratton_finance_register_config(){
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonAPIUseString' );
		register_setting( 'stratton-finance', 'StrattonAPIUseString', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonAPIPassString' );
		register_setting( 'stratton-finance', 'StrattonAPIPassString', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonRCID' );
		register_setting( 'stratton-finance', 'StrattonRCID', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonUTMSource' );
		register_setting( 'stratton-finance', 'StrattonUTMSource', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonUTMCampaign' );
		register_setting( 'stratton-finance', 'StrattonUTMCampaign', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonPriceMetaField' );
		register_setting( 'stratton-finance', 'StrattonPriceMetaField', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattontaxGroupSlug' );
		register_setting( 'stratton-finance', 'StrattontaxGroupSlug', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonCaravanCategories' );
		register_setting( 'stratton-finance', 'StrattonCaravanCategories', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonCampertrailerCategories' );
		register_setting( 'stratton-finance', 'StrattonCampertrailerCategories', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonMotorhomeCategories' );
		register_setting( 'stratton-finance', 'StrattonMotorhomeCategories', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonNewjQueryTheme' );
		register_setting( 'stratton-finance', 'StrattonNewjQueryTheme', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonBootstrapBasedThemeJS' );
		register_setting( 'stratton-finance', 'StrattonBootstrapBasedThemeJS', $args );
		unset($args);
		
		$args = array(
			'type' => 'string',
			'sanitize_callback' => 'sanitize_text_field',
			'default' => NULL,
		);
		add_site_option( 'StrattonBootstrapBasedThemeCSS' );
		register_setting( 'stratton-finance', 'StrattonBootstrapBasedThemeCSS', $args );
		unset($args);
	}
}
