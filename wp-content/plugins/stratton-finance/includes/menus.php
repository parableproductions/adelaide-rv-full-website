<?php
if (!function_exists('stratton_finance_admin_menu')){
	function stratton_finance_admin_menu(){
		add_menu_page( 'Stratton Finance Dashboard', 'Stratton Finance', 'manage_options', 'stratton-finance', 'stratton_finance_dashboard', 'dashicons-analytics' );
		add_submenu_page( 'stratton-finance', 'Stratton Finance Dashboard', 'Dashboard', 'manage_options', 'stratton-finance', 'stratton_finance_dashboard');
		add_submenu_page( 'stratton-finance', 'Config', 'Stratton Finance Config', 'manage_options', 'config', 'stratton_finance_config');
	}
	// If plugin is Netwrok Activated Use this Menu
	if(is_plugin_active_for_network($pluginfile)){
		add_action('network_admin_menu', 'stratton_finance_admin_menu');
	}
	// If plugin is Site Activated Use this Menu
	else {
		add_action( 'admin_menu' , 'stratton_finance_admin_menu' );
	}
}
