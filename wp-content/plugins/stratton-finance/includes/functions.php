<?php
function find_assetType($termslug){
	$caravancategories = get_site_option('StrattonCaravanCategories');
	$campertrailercategories = get_site_option('StrattonCampertrailerCategories');
	$motorhomecategories = get_site_option('StrattonMotorhomeCategories');
	
	if(in_array($termslug, $caravancategories)){
		return "Caravan";
	}
	elseif(in_array($termslug, $campertrailercategories)){
		return "CamperTrailer";
	}
	elseif(in_array($termslug, $motorhomecategories)){
		return "MotorhomeRV";
	}
	else {
		return 'error';
	}
}