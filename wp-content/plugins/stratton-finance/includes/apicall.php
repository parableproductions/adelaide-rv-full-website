<?php
function stratton_repayment_estimator( $args ) {
	global $wp_query;
	$baseurl = "https://sfqs.strattonfinance.com.au/WebServices/LeisureFinance/MarineAndLeisureQuote.aspx"; //Base API URL
	$allowedAssetTypes = array('Caravan','CamperTrailer','LifestyleVehicle','Marine','MarineBoat','MarineOther','Motorbike','MotorhomeRV','Trailer'); //Approved Asset Types
	$assetTypestoURL = array(
		'Caravan'			=> 'Caravan',
		'CamperTrailer'		=> 'Camper Trailer',
		'LifestyleVehicle'	=> 'Large Yacht / Luxury Yacht',
		'Marine'			=> 'Boat',
		'MarineBoat'		=> 'Small Yacht / Sailboat',
		'MarineOther'		=> 'Jet Ski',
		'Motorbike'			=> 'Dirt Bike',
		'MotorhomeRV'		=> 'Motorhome / RV',
		'Trailer'			=> 'CamperTrailer',
	);
	
	$allowedpurchasecodes = array('broker','auctionTraditional','auctionOnline','dealer','privateSeller','refinance','dealerFranchised','dealerNonFranchised'); //Approved Purchase Codes
	$apiuser = get_site_option('StrattonAPIUseString'); //API Username - Get value from saved option
	$apipass = get_site_option('StrattonAPIPassString'); //API password - get value from saved option
	$pricemeta = get_site_option('StrattonPriceMetaField'); //Pice meta Feild - Get value from saved option
	$taxgroupslug = get_site_option('StrattontaxGroupSlug'); //Taxonomy Group/Name - 
	
	$postid = $wp_query->post->ID; //Get current post ID
	$price = empty($args['amount']) ? number_format(get_post_meta($postid, $pricemeta, true),0, '.', '') : number_format($args['amount'],0, '.', ''); // get current post ID, if defined the shortcode amount will override the meta amount
	$termslug = get_the_terms($postid,$taxgroupslug)[0]->slug; //get slug of post (the first slug result if post has multiple)
	$assettype = empty($args['assettype']) ? find_assetType($termslug) : $args['assettype']; //set assettype - use code set in shortcode by default, otherwise use metavalue
	if(!in_array($assettype, $allowedAssetTypes)){
		$assettype = NULL; //if assettype is invalid set assettype as NULL
	}
	$querystring = array();
	if(!empty($assettype)){
		if(!empty($price)){
			if(!empty($apiuser) && !empty($apipass)){ //check to ensure required API fields are set
				$querystring['assetType'] = "assetType=".$assettype;
				$querystring['amountToFinance'] = "amountToFinance=".$price;
				$querystring['apiuser'] = "username=".$apiuser;
				$querystring['apipass'] = "password=".$apipass;
				
				$error = "10x00"; //no error, executed successfully
			
			}// create query strings and add to array
			else{
				$error = "44x50"; //API username and/or password not set
			}
		}
		else{
			$error = "87x30"; //Price/Amount not set
		}
	}
	else{
		$error = "21x90"; //Asset Type not set
	}
	if($error == "10x00"){
		$count = 0;
		$apiurlquery = $baseurl;
		foreach($querystring as $string){ // combine all query strings into one URL
			if($count === 0){
				$apiurlquery .= "?".$string;
			}
			else {
				$apiurlquery .= "&".$string;
			}
			$count++;
		}
		if(isset($apiurlquery)){
			$jsoncallback = file_get_contents($apiurlquery); //get JSON result
			$arrayconversion = json_decode($jsoncallback, true); //convert JSON to array
			$arrayconversion['assetType'] = $assettype;
			$arrayconversion['inputPrice'] = $price;
			$arrayconversion['StrattonRCID'] = get_site_option('StrattonRCID');
			$arrayconversion['StrattonUTMSource'] = get_site_option('StrattonUTMSource');
			$arrayconversion['StrattonUTMCampaign'] = get_site_option('StrattonUTMCampaign');
			$arrayconversion['assetRadioBtn'] = urlencode($assetTypestoURL[$assettype]);
			
			if(number_format($arrayconversion['weeklyRepayments'],0, '.', '') > 0){
				$visualdisplay = finance_output_style_one($arrayconversion); //output Stratton HTML
				echo $visualdisplay;
			}
			else{
				$error = "01x50"; // Input price too low, unable to calculate finance amount
				if(current_user_can( 'manage_options' )){
					echo "<p class=\"text-danger\">Oops... an error has occurred. Please report error code '<b>".$error ."'</b> to an administrator.</p>";
				}
			}
		}
		else{
			$error = "94x20"; //API query not set
			if(current_user_can( 'manage_options' )){
				echo "<p class=\"text-danger\">Oops... an error has occurred. Please report error code '<b>".$error ."'</b> to an administrator.</p>";
			}
		}
	}
	else{
		if(current_user_can( 'manage_options' )){
			echo "<p class=\"text-danger\">Oops... an error has occurred. Please report error code '<b>".$error ."'</b> to an administrator.</p>";
		}
	}
}