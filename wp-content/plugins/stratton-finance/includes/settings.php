<?php
if (!function_exists('stratton_finance_config')){
	function stratton_finance_config(){
		if (isset($_POST['apiUseString'])) {
			$strattonusername = $_POST['apiUseString'];
			update_site_option('StrattonAPIUseString', $strattonusername);
		}
		if (isset($_POST['apiPassString'])) {
			$strattonpassword = $_POST['apiPassString'];
			update_site_option('StrattonAPIPassString', $strattonpassword);
		}
		if (isset($_POST['priceMetaField'])) {
			$strattonmetaprice = $_POST['priceMetaField'];
			update_site_option('StrattonPriceMetaField', $strattonmetaprice);
		}
		if (isset($_POST['strattonrcid'])) {
			$strattonrcid = $_POST['strattonrcid'];
			update_site_option('StrattonRCID', $strattonrcid);
		}
		if (isset($_POST['utmsource'])) {
			$utmsource = $_POST['utmsource'];
			update_site_option('StrattonUTMSource', $utmsource);
		}
		if (isset($_POST['utmcampaign'])) {
			$utmcampaign = $_POST['utmcampaign'];
			update_site_option('StrattonUTMCampaign', $utmcampaign);
		}
		if (isset($_POST['taxGroupSlug'])) {
			$strattontaxgroupslug = $_POST['taxGroupSlug'];
			update_site_option('StrattontaxGroupSlug', $strattontaxgroupslug);
		}
		if (isset($_POST['caravanCategories'])) {
			$strattoncaravancategories = $_POST['caravanCategories'];
			update_site_option('StrattonCaravanCategories', $strattoncaravancategories);
		}
		if (isset($_POST['campertrailerCategories'])) {
			$strattoncampertrailercategories = $_POST['campertrailerCategories'];
			update_site_option('StrattonCampertrailerCategories', $strattoncampertrailercategories);
		}
		if (isset($_POST['motorhomeCategories'])) {
			$strattonmotorhomecategories = $_POST['motorhomeCategories'];
			update_site_option('StrattonMotorhomeCategories', $strattonmotorhomecategories);
		}
		if (isset($_POST['newjQueryTheme'])) {
			$strattonjquerytheme = $_POST['newjQueryTheme'];
			update_site_option('StrattonBootstrapBasedThemeCSS', $strattonjquerytheme);
		}
		if (isset($_POST['bootstrapThemeJS'])) {
			$strattonbootstrapthemejs = $_POST['bootstrapThemeJS'];
			update_site_option('StrattonBootstrapBasedThemeJS', $strattonbootstrapthemejs);
		}
		if (isset($_POST['bootstrapThemeCSS'])) {
			$$strattonbootstrapthemecss = $_POST['bootstrapThemeCSS'];
			update_site_option('StrattonNewjQueryTheme', $$strattonbootstrapthemecss);
		}
		$taxgroupslug = get_site_option('StrattontaxGroupSlug');

		// If plugin is Netwrok Activated Get Taxonomies from blog 1
		if(is_plugin_active_for_network($pluginfile)){
			switch_to_blog(1);
			$taxonomies = get_taxonomies();
			$terms = get_terms(array(
				'taxonomy' => $taxgroupslug,
				'hide_empty' => false,
			));
			restore_current_blog();
		}
		// If plugin is Site Activated Get standard taxonomies
		else {
			$taxonomies = get_taxonomies();
			$terms = get_terms(array(
				'taxonomy' => $taxgroupslug,
				'hide_empty' => false,
			));
		}
		$caravancategories = get_site_option('StrattonCaravanCategories');
		$campertrailercategories = get_site_option('StrattonCampertrailerCategories');
		$motorhomecategories = get_site_option('StrattonMotorhomeCategories');
		?>
		<div class="wrap">
			<h1>Config</h1>
			<div class="contianer">
				<div class="col-md-5">
					<h2>Stratton Finance Plugin Config</h2>
					<form method="post" action="admin.php?page=config">
						<?php settings_fields( 'stratton-finance' ); ?>
						<?php do_settings_sections( 'stratton-finance' ); ?>
						<div class="form-group">
							<label for="apiUseString">API Username</label>
							<input type="text" class="form-control" id="apiUseString" name="apiUseString" value="<?php echo get_site_option('StrattonAPIUseString');?>" required/>
						</div>
						<div class="form-group">
							<label for="apiPassString">API Password</label>
							<input type="text" class="form-control" id="apiPassString" name="apiPassString" value="<?php echo get_site_option('StrattonAPIPassString');?>" required/>
						</div>
						<div class="form-group">
							<label for="strattonrcid">Stratton RCID</label>
							<input type="text" class="form-control" id="strattonrcid" name="strattonrcid" value="<?php echo get_site_option('StrattonRCID');?>" required/>
						</div>
						<div class="form-group">
							<label for="utmsource">UTM Source</label>
							<input type="text" class="form-control" id="utmsource" name="utmsource" value="<?php echo get_site_option('StrattonUTMSource');?>" required/>
						</div>
						<div class="form-group">
							<label for="utmcampaign">UTM Campaign</label>
							<input type="text" class="form-control" id="utmcampaign" name="utmcampaign" value="<?php echo get_site_option('StrattonUTMCampaign');?>" required/>
						</div>
						<div class="form-group">
							<label for="priceMetaField">Price Meta Field</label>
							<input type="text" class="form-control" id="priceMetaField" name="priceMetaField" value="<?php echo get_site_option('StrattonPriceMetaField');?>" required/>
						</div>
						<div class="form-group">
							<label for="taxGroupSlug">Taxonomy Name</label>
							<select class="form-control" id="taxGroupSlug" name="taxGroupSlug" required>
							<option value=""<?php if(empty(get_site_option('StrattontaxGroupSlug'))){echo " selected";}?> disabled>-- Select One --</option>
							<?php
							if ( $taxonomies ) {
								foreach ( $taxonomies  as $taxonomy ) {
									if($taxonomy):
										?>
										<option value="<?php echo $taxonomy;?>"<?php if(get_site_option('StrattontaxGroupSlug') == $taxonomy){echo " selected";}?>><?php echo $taxonomy;?></option>
										<?php
									endif;
								}
							}
							?>
						</select>
						</div>
						<div class="form-group">
							<label for="caravanCategories">Caravan Terms</label>
							<select multiple class="form-control" id="caravanCategories" name="caravanCategories[]">
							<?php
							if ( $terms ) {
								foreach ( $terms  as $term ) {
									if($term):
										?>
										<option value="<?php echo $term->slug;?>"<?php if(in_array($term->slug, $caravancategories, false)){echo " selected";}?>><?php echo $term->name;?></option>
										<?php
									endif;
								}
							}
							?>
						</select>
						</div>
						<div class="form-group">
							<label for="campertrailerCategories">Camper Trailer Terms</label>
							<select multiple class="form-control" id="campertrailerCategories" name="campertrailerCategories[]">
							<?php
							if ( $terms ) {
								foreach ( $terms  as $term ) {
									if($term):
										?>
										<option value="<?php echo $term->slug;?>"<?php if(in_array($term->slug, $campertrailercategories, false)){echo " selected";}?>><?php echo $term->name;?></option>
										<?php
									endif;
								}
							}
							?>
						</select>
						</div>
						<div class="form-group">
							<label for="motorhomeCategories">Motorhome Terms</label>
							<select multiple class="form-control" id="motorhomeCategories" name="motorhomeCategories[]">
							<?php
							if ( $terms ) {
								foreach ( $terms  as $term ) {
									if($term):
										?>
										<option value="<?php echo $term->slug;?>"<?php if(in_array($term->slug, $motorhomecategories, false)){echo " selected";}?>><?php echo $term->name;?></option>
										<?php
									endif;
								}
							}
							?>
							</select>
						</div>
						<hr>
						<h3 class="text-danger">Advanced Settings</h3>
						<div class="form-group">
							<label for="newjQueryTheme">Load jQuery 2.2.4</label>
							<select class="form-control" id="newjQueryTheme" name="newjQueryTheme">
								<option value="true"<?php if(get_site_option('StrattonNewjQueryTheme') == '' || get_site_option('StrattonNewjQueryTheme') == 'true'){echo " selected";} ?>>Yes</option>
								<option value="false"<?php if(get_site_option('StrattonNewjQueryTheme') == 'false'){echo " selected";} ?>>No</option>
							</select>
							<p class="help-block">Disable this if your theme uses a version of jQuery equal to or greater than verson 2.2.4, ask your website designer/developer if unsure</p>
						</div>
						<div class="form-group">
							<label for="bootstrapThemeJS">Load Bootstrap 3 Core Javascript</label>
							<select class="form-control" id="bootstrapThemeJS" name="bootstrapThemeJS">
								<option value="true"<?php if(get_site_option('StrattonBootstrapBasedThemeJS') == '' || get_site_option('StrattonBootstrapBasedThemeJS') == 'true'){echo " selected";} ?>>Yes</option>
								<option value="false"<?php if(get_site_option('StrattonBootstrapBasedThemeJS') == 'false'){echo " selected";} ?>>No</option>
							</select>
							<p class="help-block">Disable this if your theme is based on Bootstrap 3, ask your website designer/developer if unsure.</p>
						</div>
						<div class="form-group">
							<label for="bootstrapThemeCSS">Load Bootstrap 3 Core CSS</label>
							<select class="form-control" id="bootstrapThemeCSS" name="bootstrapThemeCSS">
								<option value="true"<?php if(get_site_option('StrattonBootstrapBasedThemeCSS') == '' || get_site_option('StrattonBootstrapBasedThemeCSS') == 'true'){echo " selected";} ?>>Yes</option>
								<option value="false"<?php if(get_site_option('StrattonBootstrapBasedThemeCSS') == 'false'){echo " selected";} ?>>No</option>
							</select>
							<p class="help-block">Disable this if your theme is based on Bootstrap 3, ask your website designer/developer if unsure.</p>
						</div>
						<?php submit_button(); ?>
					</form>
				</div>
				<div class="col-md-5">
					<pre><?php print_r(get_site_option('StrattonNewjQueryTheme')); ?></pre>
					<pre><?php print_r(get_site_option('StrattonBootstrapBasedThemeJS')); ?></pre>
					<pre><?php print_r(get_site_option('StrattonBootstrapBasedThemeCSS')); ?></pre>
				</div>
			</div>
		</div>
		<?php
	}
}