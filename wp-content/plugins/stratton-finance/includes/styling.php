<?php

if ( is_admin() ){
	// admin actions
	add_action( 'admin_head' , 'stratton_finance_admin_register_head' );
	add_action( 'admin_enqueue_scripts' , 'stratton_finance_backend_scripts' );
} else {
	// non-admin enqueues, actions, and filters
	add_action( 'wp_enqueue_scripts', 'stratton_finance_frontend_scripts');
}

function stratton_finance_frontend_scripts(){
	
	if(!get_site_option('StrattonNewjQueryTheme')){
		wp_deregister_script('jquery');
		wp_enqueue_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js" , array(), '2.2.4' );
	}
	if(!get_site_option('StrattonBootstrapBasedThemeCSS')){
		wp_enqueue_style( 'stratton-finance-bootstrap', plugins_url( '../css/stratton-bs.min.css', __FILE__ ), array(), '3.3.7' );
	}
	if(!get_site_option('StrattonBootstrapBasedThemeJS')){
		wp_enqueue_script( 'stratton-finance-bootstrap-js', plugins_url( '../js/stratton-bs.min.js', __FILE__ ), array('jquery'), '3.3.7' );
	}
	wp_enqueue_style( 'stratton-finance-frontend', plugins_url( '../css/frontend.css', __FILE__ ), array(), '1.0.0' );
}

function stratton_finance_backend_scripts(){
	wp_enqueue_style( 'stratton-finance-bootstrap', plugins_url( '../css/stratton-bs.min.css', __FILE__ ), array(), '3.3.7' );
	wp_enqueue_style( 'stratton-finance-backend', plugins_url( '../css/backend.css', __FILE__ ), array(), '1.0.0' );
}
function stratton_finance_admin_register_head() {
	//$url = plugins_url('stratton-finance/css/backend.css');
	//echo "<link rel='stylesheet' type='text/css' href='$url' />\n";
}
