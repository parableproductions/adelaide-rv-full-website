<?php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
defined( 'ABSPATH' ) OR exit;
require_once 'includes/functions.php';
/*
Plugin Name: Stratton Finance Calculator
Description: Finance calculator widget for Statton Financial Services.
Author: Parable Productions Pty Ltd
Version: 1.0.0
*/
$pluginfile = plugin_dir_path( __FILE__ );
if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
	require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
}
/**
###############################
#
# Plugin On Activate - Check for Dependancies
#
###############################
*/
function stratton_finance_on_activation() {
	
}
register_activation_hook( __FILE__, 'stratton_finance_on_activation' );

function stratton_finance_on_deactivation() {
	
}
register_deactivation_hook( __FILE__, 'stratton_finance_on_deactivation' );

require_once 'includes/styling.php'; //load plugin dashboard and frontend styling

require_once 'includes/menus.php'; //setup menus and call functions to load menu pages

require_once 'includes/dashboard.php'; //dashboard page as called by menu function

require_once 'includes/settings.php'; //settings/config page as called by menu function

require_once 'includes/preferences.php'; //register plugin settings/preferences

require_once 'includes/styleone.php'; //load first module output style

require_once 'includes/apicall.php'; //define the api call and return output based on settings

add_shortcode( 'stratton_repayment', 'stratton_repayment_estimator' ); //define shortcode to load module inline within wordpress content editor
add_action( 'stratton_repayment', 'stratton_repayment_estimator' ); //define action hook to allow designer/developer to call plugin within theme files

